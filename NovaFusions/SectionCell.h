//
//  SectionCell.h
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *title, *description;
@property (nonatomic, retain) UIView *bgview;

@end
