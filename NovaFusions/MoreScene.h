//
//  MoreScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "Reachability.h"
#import "GlobalFunction.h"
#import "AppDelegate.h"
#import "HelpCell.h"

@interface MoreScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
{
    HelpCell *helpcell;
    BOOL isconnected;
    UIAlertView *alert;
    NSTimer *connectiontimer;
}

@property (retain, nonatomic) IBOutlet UIImageView *topbgimageview;
@property (retain, nonatomic) IBOutlet UIImageView *arrowimageview;
@property (retain, nonatomic) IBOutlet UIView *aboutview;
@property (retain, nonatomic) IBOutlet UIView *contactusview;

@property (retain, nonatomic) IBOutlet UIView *helpview;
@property (retain, nonatomic) IBOutlet UITableView *helptable;
@property (retain, nonatomic) IBOutlet UIButton *closebutton;

-(IBAction)about:(id)sender;
-(IBAction)contactus:(id)sender;
-(IBAction)help:(id)sender;
-(IBAction)close:(id)sender;

//contactus//
@property (retain, nonatomic) IBOutlet UIImageView *contactusimageview;

-(IBAction)twittercontactus:(id)sender;
-(IBAction)facebookcontactus:(id)sender;
-(IBAction)linkedcontactus:(id)sender;

@end
