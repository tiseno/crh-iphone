//
//  BookmarkCell.m
//  NovaFusions
//
//  Created by tiseno on 10/25/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "BookmarkCell.h"

@implementation BookmarkCell

@synthesize titlelabel, codelabel, productimage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 220, 40)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor blackColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:14];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.titlelabel = Title;
        [Title release];
        [self addSubview:titlelabel];
        
        UILabel *Code = [[UILabel alloc] initWithFrame:CGRectMake(90, 50, 220, 20)];
        Code.textAlignment = UITextAlignmentLeft;
        Code.textColor = [UIColor grayColor];
        Code.backgroundColor = [UIColor clearColor];
        Code.font = [UIFont systemFontOfSize:11];
        Code.lineBreakMode = UILineBreakModeWordWrap;
        Code.numberOfLines = 1;
        self.codelabel = Code;
        [Code release];
        [self addSubview:codelabel];
        
        UIImageView *ProductImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 83, 104)];
        self.productimage = ProductImage;
        [ProductImage release];
        [self addSubview:productimage];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [titlelabel release];
    [codelabel release];
    [productimage release];
    [super dealloc];
}

@end
