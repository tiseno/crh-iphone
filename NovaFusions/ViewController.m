//
//  ViewController.m
//  NovaFusions
//
//  Created by tiseno on 10/22/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

@synthesize mainArr;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSelector:@selector(delay) withObject:nil afterDelay:1];
}

-(void)delay
{
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

//checking connection//
-(void)checkingconnection{
    if([GlobalFunction NetworkStatus]){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
        [connectiontimer invalidate];
        [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(void)delay2
{
    WebServices *ws = [[WebServices alloc] init];
    
    mainArr = [[ws selectAllFromApplicationTableExceptParam] retain];
    
    [ws release];
    MainScene *GtestClasssViewController=[[[MainScene alloc] initWithNibName:@"MainScene"  bundle:nil] autorelease];
    GtestClasssViewController.mainArr = self.mainArr;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

@end
