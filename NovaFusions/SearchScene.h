//
//  SearchScene.h
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "WebServices.h"
#import "GlobalFunction.h"
#import "SearchCell.h"
#import "ProductScene.h"

@interface SearchScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    SearchCell *searchcell;
    BOOL isconnected, iswhite;
    int itemselected, total, current;
    NSTimer *connectiontimer;
    UIAlertView *alert;
    
    NSMutableArray *itemNameArray, *itemCodeArray, *itemPriceArray, *itemImageArray, *itemAppIdArray, *itemIdArray;
    
    UIView *searchheaderview;
}

@property (retain, nonatomic) IBOutlet UIToolbar *topbar;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UITableView *resulttable;
@property (retain, nonatomic) IBOutlet UILabel *keywordlabel;
@property (retain, nonatomic) IBOutlet UIView *searchview;
@property (retain, nonatomic) IBOutlet UISearchBar *searchbar;
@property (retain, nonatomic) IBOutlet UIView *headerview;

-(IBAction)back:(id)sender;
-(IBAction)cancel:(id)sender;
-(IBAction)search:(id)sender;

@property (retain, nonatomic) NSString *keyword;

@property (retain, nonatomic) NSMutableArray *itemNameArr, *itemCodeArr, *itemPriceArr, *itemImageArr, *itemAppIdArr, *itemIdArr;

@end
