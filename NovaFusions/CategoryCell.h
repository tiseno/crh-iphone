//
//  CategoryCell.h
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CategoryCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UIView *separator;
@property (nonatomic, retain) UIButton *thumbnailbutton;
@property (nonatomic, retain) CAGradientLayer *gradient;

@end
