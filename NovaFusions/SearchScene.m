//
//  SearchScene.m
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define SearchTableHeight 105

#import "SearchScene.h"

@implementation SearchScene

@synthesize topbar, titlelabel, resulttable, searchbar, keywordlabel, keyword, searchview, headerview;

@synthesize itemCodeArr, itemImageArr, itemNameArr, itemPriceArr, itemAppIdArr, itemIdArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.resulttable.tableFooterView = [[[UIView alloc] init] autorelease];
    
    if([[UIScreen mainScreen] bounds].size.height > 480)
        self.topbar.frame = CGRectMake(0, 0, 320, 44);
    
    [self.topbar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"more_top_bar.png"]] autorelease] atIndex:1];
    
    self.titlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
    
    searchheaderview = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 39)] autorelease];
    [searchheaderview addSubview:self.headerview];
    self.resulttable.tableHeaderView = searchheaderview;
    self.headerview.backgroundColor = [UIColor colorWithRed:206.0/255.0 green:208.0/255.0 blue:216.0/255.0 alpha:1.0];
    self.searchview.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:58.0/255.0 blue:67.0/255.0 alpha:1.0];
    self.keywordlabel.text = self.keyword;
    
    [[self.searchbar.subviews objectAtIndex:0] removeFromSuperview];
    
    total = self.itemNameArr.count;
    if(total > 50)
    {
        current = 50;
    }
    else
    {
        current = total;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [topbar release];
    [titlelabel release];
    [resulttable release];
    [searchbar release];
    [keywordlabel release];
    [searchview release];
    [headerview release];
    
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return current;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    searchcell = (SearchCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(searchcell == nil){
        searchcell = [[[SearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    
    searchcell.frame = CGRectMake(0, 0, 320, SearchTableHeight);
    searchcell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    searchcell.titlelabel.text = [NSString stringWithFormat:@"%@", [self.itemNameArr objectAtIndex:indexPath.row]];
    searchcell.codelabel.text = [NSString stringWithFormat:@"CODE: %@", [self.itemCodeArr objectAtIndex:indexPath.row]];
    searchcell.retaillabel.text = [NSString stringWithFormat:@"RETAIL: $%@", [self.itemPriceArr objectAtIndex:indexPath.row]];
    searchcell.productimage.imageURL = [NSURL URLWithString:[self.itemImageArr objectAtIndex:indexPath.row]];
    
    if(indexPath.row % 2 != 0)
        searchcell.contentView.backgroundColor = [UIColor whiteColor];
    else
        searchcell.contentView.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:243.0/255.0 blue:244.0/255.0 alpha:1.0];

    return searchcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    searchcell = (SearchCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    itemselected = indexPath.row;
    [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    searchcell= (SearchCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return searchcell.frame.size.height;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    
    if (maximumOffset - currentOffset <= -20) {
        if(current < total)
        {
            if(total-current < 50)
                current += (total-current);
            else
                current +=50;
            [self.resulttable reloadData];
        }
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar
{
    [self.searchbar resignFirstResponder];
    self.resulttable.hidden = YES;
    iswhite = YES;
    [GlobalFunction AddLoadingScreen:self];
    [self performSelector:@selector(delay3) withObject:nil afterDelay:0.1];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchbar resignFirstResponder];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)cancel:(id)sender
{
    searchheaderview.frame = CGRectMake(0, 0, 320, 39);
    [self.searchview removeFromSuperview];
    self.headerview.frame = CGRectMake(0, 0, 320, 39);
    self.resulttable.tableHeaderView = searchheaderview;
}

-(IBAction)search:(id)sender
{
    searchheaderview.frame = CGRectMake(0, 0, 320, 83);
    [searchheaderview addSubview:self.searchview];
    self.headerview.frame = CGRectMake(0, 44, 320, 39);
    self.resulttable.tableHeaderView = searchheaderview;
    [self.resulttable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(void)delay2
{
    ProductScene *GtestClasssViewController=[[[ProductScene alloc] initWithNibName:@"ProductScene"  bundle:nil] autorelease];
    GtestClasssViewController.appID = [itemAppIdArr objectAtIndex:itemselected];
    GtestClasssViewController.itemID = [itemIdArr objectAtIndex:itemselected];
    GtestClasssViewController.isjumpfromsearch = YES;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(void)delay3
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSArray *result = [ws searchItemByKeywords:self.searchbar.text];
    
    if(result.count > 0)
    {
        itemCodeArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemImageArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemNameArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemPriceArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemAppIdArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemIdArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        for(int j = 0; j < result.count; j++)
        {
            NSDictionary* novainfo = [result objectAtIndex:j];
            
            NSDictionary* novadetails = [novainfo objectForKey:@"item"];
            
            [itemNameArray addObject:[novadetails objectForKey:@"name"]];
            
            NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the price of the product
            NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
            NSDictionary *price = [priceDetails objectForKey:@"0"];
            
            [itemPriceArray addObject:[price objectForKey:@"value"]];
            
            NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            [itemCodeArray addObject:[itemCode objectForKey:@"value"]];
            
            NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            [itemImageArray addObject:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [itemAppIdArray addObject:[novadetails objectForKey:@"application_id"]];
            
            [itemIdArray addObject:[novadetails objectForKey:@"id"]];
        }
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    self.keywordlabel.text = self.searchbar.text;
    self.itemCodeArr = itemCodeArray;
    self.itemImageArr = itemImageArray;
    self.itemNameArr = itemNameArray;
    self.itemPriceArr = itemPriceArray;
    self.itemAppIdArr = itemAppIdArray;
    self.itemIdArr = itemIdArray;
    
    total = self.itemNameArr.count;
    if(total > 50)
    {
        current = 50;
    }
    else
        current = total;
    
    [self.resulttable reloadData];
    [self.resulttable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    self.resulttable.hidden = NO;
    
    searchheaderview.frame = CGRectMake(0, 0, 320, 44);
    [self.searchview removeFromSuperview];
    self.headerview.frame = CGRectMake(0, 0, 320, 44);
    self.resulttable.tableHeaderView = searchheaderview;
}

@end
