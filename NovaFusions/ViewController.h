//
//  ViewController.h
//  NovaFusions
//
//  Created by tiseno on 10/22/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalFunction.h"
#import "WebServices.h"
#import "MainScene.h"

@interface ViewController : UIViewController
{
    BOOL isconnected;
    NSTimer *connectiontimer;
    UIAlertView *alert;
}

@property (retain, nonatomic) NSArray *mainArr;

@end
