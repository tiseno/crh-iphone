//
//  SearchCell.h
//  NovaFusions
//
//  Created by tiseno on 10/31/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *titlelabel, *codelabel, *retaillabel;
@property (nonatomic, retain) UIImageView *productimage;

@end
