//
//  ProductScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "AsyncImageView.h"
#import "Reachability.h"
#import "Toast+UIView.h"
#import "AppDelegate.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "DatabaseAction.h"
#import "ProductCell.h"
#import "JobCell.h"

@interface ProductScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate>
{
    ProductCell *productcell;
    JobCell *jobcell;
    BOOL isconnected, istechnical, isneedprice, isadd, isexist, issubcribe;
    float red, green, blue;
    int productrowselected, numberlayer, inviewpage, buttontag;
    NSString *imagepathM, *titleOfRowSelected, *productCodeOfRowSelected;
    UIAlertView *alert, *alertenquiry, *alertaddjob;
    NSTimer *connectiontimer, *checkimagetimer;
    UILabel *descriptionlabel;
    NSArray *joblist;
    UITextField *newjobtitletextfield, *quantitytextfield;
    UIView *transparentview;
    UIImageView *shadowimage;
    
    CGAffineTransform detailtransform, jobtransform, enquirytransform, pricetransform, typetransform;
    
    DatabaseAction *db;
}

@property (retain, nonatomic) IBOutlet UIView *headerview;
@property (retain, nonatomic) IBOutlet UIImageView *categoryheaderbg;
@property (retain, nonatomic) IBOutlet UITableView *producttable;
@property (retain, nonatomic) IBOutlet UILabel *categorylabel;

-(IBAction)back:(id)sender;

@property (retain, nonatomic) NSString *categorytext, *appID, *itemID;
@property (retain, nonatomic) NSArray *itemArr;

@property (retain, nonatomic) NSMutableArray *totalItem;
@property (retain, nonatomic) NSMutableArray *itemPriceArr;
@property (retain, nonatomic) NSMutableArray *middleImageArr;
@property (retain, nonatomic) NSMutableArray *tradeArr;
@property (nonatomic) int row, rowselected;
@property (nonatomic) BOOL isjump, issearch, isjumpfromsearch;

//detailview//
@property (retain, nonatomic) IBOutlet UIView *detailview;
@property (retain, nonatomic) IBOutlet UIImageView *productimage;
@property (retain, nonatomic) IBOutlet UIImageView *buttonbgimage;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UIImageView *detailbgimage;
@property (retain, nonatomic) IBOutlet UIScrollView *detailscrollview;
@property (retain, nonatomic) IBOutlet UILabel *codelabel;
@property (retain, nonatomic) IBOutlet UILabel *tradelabel;
@property (retain, nonatomic) IBOutlet UILabel *retaillabel;
@property (retain, nonatomic) IBOutlet UILabel *seriallabel;
@property (retain, nonatomic) IBOutlet UIImageView *titlebgimage;
@property (retain, nonatomic) IBOutlet UIImageView *detailshadowimage;

-(IBAction)zoomin:(id)sender;
-(IBAction)drawing:(id)sender;
-(IBAction)bookmarks:(id)sender;
-(IBAction)add:(id)sender;
-(IBAction)enquiry:(id)sender;
-(IBAction)email:(id)sender;

@property (retain, nonatomic) NSArray *technicalImageArr;
@property (retain, nonatomic) NSArray *galleryImageArr;
@property (retain, nonatomic) NSString *descriptiontext;
//////////////

//popupimage//
@property (retain, nonatomic) IBOutlet UIView *popupview;
@property (retain, nonatomic) IBOutlet UIImageView *popupimage;
@property (retain, nonatomic) IBOutlet UILabel *popuplabel;
@property (retain, nonatomic) IBOutlet UIButton *popuppreviousbutton;
@property (retain, nonatomic) IBOutlet UIButton *popupnextbutton;
@property (retain, nonatomic) IBOutlet UIButton *popupclosebutton;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loadingview;

-(IBAction)closepopup:(id)sender;
-(IBAction)next:(id)sender;
-(IBAction)previous:(id)sender;
//////////////

//addnewjob//
@property (retain, nonatomic) IBOutlet UILabel *jobcodelabel;
@property (retain, nonatomic) IBOutlet UILabel *jobtitlelabel;
@property (retain, nonatomic) IBOutlet UILabel *jobfixlabel;
@property (retain, nonatomic) IBOutlet UIImageView *jobproductimage;
@property (retain, nonatomic) IBOutlet UITableView *jobtable;
@property (retain, nonatomic) IBOutlet UIView *jobview;
@property (retain, nonatomic) IBOutlet UIImageView *jobshadowimage;

-(IBAction)newjob:(id)sender;
-(IBAction)donejob:(id)sender;

//enquiry//
@property (retain, nonatomic) IBOutlet UIView *enquiryview;
@property (retain, nonatomic) IBOutlet UITextField *popfirstnametextfield;
@property (retain, nonatomic) IBOutlet UITextField *poplastnametextfield;
@property (retain, nonatomic) IBOutlet UITextField *popemailtextfield;
@property (retain, nonatomic) IBOutlet UITextField *popphonetextfield;
@property (retain, nonatomic) IBOutlet UITextField *popaddresstextfield;
@property (retain, nonatomic) IBOutlet UITextField *popcompanytextfield;
@property (retain, nonatomic) IBOutlet UITextField *popproductcodetextfield;
@property (retain, nonatomic) IBOutlet UITextView *popcommentstextview;
@property (retain, nonatomic) IBOutlet UILabel *poptitlelabel;
@property (retain, nonatomic) IBOutlet UIScrollView *popscrollview;
@property (retain, nonatomic) IBOutlet UIImageView *popshadowimage;
@property (retain, nonatomic) IBOutlet UIButton *popselectedbutton;
@property (retain, nonatomic) IBOutlet UIButton *popnotselectedbutton;

-(IBAction)subcribe:(id)sender;
-(IBAction)submit:(id)sender;
-(IBAction)cancel:(id)sender;
///////////

//email//
@property (retain, nonatomic) IBOutlet UIView *priceview;
@property (retain, nonatomic) IBOutlet UIView *typeview;

-(IBAction)cancelemail:(id)sender;
-(IBAction)sendwithprice:(id)sender;
-(IBAction)sendwithoutprice:(id)sender;
-(IBAction)sendplain:(id)sender;
-(IBAction)sendhtml:(id)sender;
-(IBAction)sendpdf:(id)sender;

@end
