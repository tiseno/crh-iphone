//
//  HelpCell.h
//  NovaFusions
//
//  Created by tiseno on 10/31/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *helpdescription;
@property (nonatomic, retain) UIImageView *imageview, *dotimage;

@end
