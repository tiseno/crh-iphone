//
//  MyJobCell.h
//  NovaFusions
//
//  Created by tiseno on 10/30/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyJobCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *myjobtitle, *myjobdate;
@property (nonatomic, retain) UIImageView *arrowimage;

@end
