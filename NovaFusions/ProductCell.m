//
//  ProductCell.m
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

@synthesize title, code, description, productimage, serial, serialimage, backgroundview;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        backgroundview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 105)];
        [self addSubview:backgroundview];
        
        UIImageView *ProductImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 83, 104)];
        self.productimage = ProductImage;
        [ProductImage release];
        [self addSubview:productimage];
        
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(90, 5, 220, 35)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor blackColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:14];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 2;
        self.title = Title;
        [Title release];
        [self addSubview:title];
        
        UILabel *Code = [[UILabel alloc] initWithFrame:CGRectMake(90, 45, 220, 10)];
        Code.textAlignment = UITextAlignmentLeft;
        Code.textColor = [UIColor grayColor];
        Code.backgroundColor = [UIColor clearColor];
        Code.font = [UIFont systemFontOfSize:12];
        Code.lineBreakMode = UILineBreakModeWordWrap;
        Code.numberOfLines = 0;
        self.code = Code;
        [Code release];
        [self addSubview:code];
        
        UILabel *Description = [[UILabel alloc] initWithFrame:CGRectMake(90, 50, 220, 50)];
        Description.textAlignment = UITextAlignmentLeft;
        Description.textColor = [UIColor grayColor];
        Description.backgroundColor = [UIColor clearColor];
        Description.font = [UIFont systemFontOfSize:12];
        Description.lineBreakMode = UILineBreakModeWordWrap;
        Description.numberOfLines = 2;
        self.description = Description;
        [Description release];
        [self addSubview:description];
        
        UIImageView *SerialImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        self.serialimage = SerialImage;
        [SerialImage release];
        [self addSubview:serialimage];
        
        UILabel *Serial = [[UILabel alloc] initWithFrame:CGRectMake(3, 10, 20, 10)];
        Serial.textAlignment = UITextAlignmentCenter;
        Serial.textColor = [UIColor whiteColor];
        Serial.backgroundColor = [UIColor clearColor];
        Serial.font = [UIFont boldSystemFontOfSize:11];
        Serial.lineBreakMode = UILineBreakModeWordWrap;
        Serial.numberOfLines = 0;
        self.serial = Serial;
        [Serial release];
        [self addSubview:serial];
        
        UIImageView *Separator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 104.5, 300, 0.5)];
        Separator.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [self addSubview:Separator];
        [Separator release];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [title release];
    [code release];
    [description release];
    [serial release];
    [productimage release];
    [serialimage release];
    [backgroundview release];
    [super dealloc];
}

@end
