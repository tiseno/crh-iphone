//
//  MyJobScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "AsyncImageView.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "DatabaseAction.h"
#import "Toast+UIView.h"
#import "Myjob.h"
#import "MyJobCell.h"
#import "InfoScene.h"

@interface MyJobScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate>
{
    MyJobCell *myjobcell;
    BOOL isconnected;
    UIAlertView *alert;
    NSTimer *connectiontimer;
    NSArray *joblist;
    UITextField *newjobtitletextfield;
    
    DatabaseAction *db;
}

@property (retain, nonatomic) IBOutlet UITableView *jobtable;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UIToolbar *topbar;

-(IBAction)addjob:(id)sender;
-(IBAction)back:(id)sender;

@end
