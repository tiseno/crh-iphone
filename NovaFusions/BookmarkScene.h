//
//  BookmarkScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "AsyncImageView.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "DatabaseAction.h"
#import "BookmarkCell.h"
#import "Bookmark.h"
#import "ProductScene.h"

@interface BookmarkScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
{
    BookmarkCell *bookmarkcell;
    BOOL isconnected, iseditbookmark, isneedprice;
    UIAlertView *alert;
    NSTimer *connectiontimer;
    NSArray *bookmarklist;
    
    CGAffineTransform pricetransform, typetransform;
    
    DatabaseAction *db;
}

@property (retain, nonatomic) IBOutlet UITableView *bookmarktable;
@property (retain, nonatomic) IBOutlet UIButton *editbookmarkbutton;
@property (retain, nonatomic) IBOutlet UIButton *backbutton;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UIView *priceview;
@property (retain, nonatomic) IBOutlet UIView *typeview;
@property (retain, nonatomic) IBOutlet UIToolbar *topbar;
@property (retain, nonatomic) IBOutlet UIToolbar *bottombar;

-(IBAction)editbookmark:(id)sender;
-(IBAction)back:(id)sender;

-(IBAction)email:(id)sender;
-(IBAction)cancel:(id)sender;
-(IBAction)sendwithprice:(id)sender;
-(IBAction)sendwithoutprice:(id)sender;
-(IBAction)sendplain:(id)sender;
-(IBAction)sendhtml:(id)sender;
-(IBAction)sendpdf:(id)sender;

@end
