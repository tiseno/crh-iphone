//
//  MainScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "ZBarSDK.h"
#import "AppDelegate.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "SectionCell.h"
#import "CategoryScene.h"
#import "BookmarkScene.h"
#import "MyJobScene.h"
#import "MoreScene.h"
#import "SearchScene.h"

@interface MainScene : UIViewController<UITableViewDataSource, UITableViewDelegate, ZBarReaderDelegate, UIImagePickerControllerDelegate, UISearchBarDelegate, UIAlertViewDelegate>
{
    SectionCell *sectioncell;
    BOOL isconnected;
    int row;
    UIAlertView *alert;
    NSTimer *connectiontimer;
    
    CGAffineTransform searchtransform;
    
    NSMutableArray *itemNameArr, *itemCodeArr, *itemPriceArr, *itemImageArr, *itemAppIdArr, *itemIdArr;
    
    NSString *data;
}

@property (nonatomic, retain) UIImagePickerController *imgPicker;
@property (retain, nonatomic) IBOutlet UITableView *sectiontable;
@property (retain, nonatomic) IBOutlet UISearchBar *searchbar;
@property (retain, nonatomic) IBOutlet UIView *bottomview;
@property (retain, nonatomic) IBOutlet UIView *subview;
@property (retain, nonatomic) IBOutlet UIView *tapview;

-(IBAction)myjobs:(id)sender;
-(IBAction)bookmarks:(id)sender;
-(IBAction)search:(id)sender;
-(IBAction)more:(id)sender;
-(IBAction)scan:(id)sender;
-(IBAction)launchsafari:(id)sender;

@property (retain, nonatomic) NSArray *mainArr;

@end
