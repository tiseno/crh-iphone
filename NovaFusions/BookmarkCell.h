//
//  BookmarkCell.h
//  NovaFusions
//
//  Created by tiseno on 10/25/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookmarkCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *titlelabel, *codelabel;
@property (nonatomic, retain) UIImageView *productimage;

@end
