//
//  InfoScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "Reachability.h"
#import "AsyncImageView.h"
#import "GlobalFunction.h"
#import "Toast+UIView.h"
#import "WebServices.h"
#import "DatabaseAction.h"
#import "InfoCell.h"
#import "Product.h"
#import "ProductScene.h"

@interface InfoScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
{
    InfoCell *infocell;
    BOOL isconnected, iseditmyjob, isneedprice;
    UIAlertView *alert;
    NSTimer *connectiontimer;
    NSArray *productlist;
    float totalretail, totaltrade;
    NSString *jobTitle;
    
    CGAffineTransform pricetransform, typetransform;
    
    DatabaseAction *db;
}

@property (retain, nonatomic) IBOutlet UITableView *infotable;
@property (retain, nonatomic) IBOutlet UILabel *joblabel;
@property (retain, nonatomic) IBOutlet UIView *totalcostview;
@property (retain, nonatomic) IBOutlet UILabel *tradelabel;
@property (retain, nonatomic) IBOutlet UILabel *retaillabel;
@property (retain, nonatomic) IBOutlet UIView *editjobview;
@property (retain, nonatomic) IBOutlet UIButton *editjobinfobutton;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UIButton *backbutton;
@property (retain, nonatomic) IBOutlet UIView *jobtitleview;
@property (retain, nonatomic) IBOutlet UITextField *jobtitletextfield;

@property (retain, nonatomic) IBOutlet UIView *priceview;
@property (retain, nonatomic) IBOutlet UIView *typeview;
@property (retain, nonatomic) IBOutlet UIToolbar *topbar;
@property (retain, nonatomic) IBOutlet UIToolbar *bottombar;

@property (retain, nonatomic) NSString *jobtitle;
@property (nonatomic) int jobrowselected;

-(IBAction)editjobinfo:(id)sender;
-(IBAction)deletejob:(id)sender;
-(IBAction)back:(id)sender;

@end
