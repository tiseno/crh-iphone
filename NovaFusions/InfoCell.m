//
//  InfoJobCell.m
//  NovaFusions
//
//  Created by tiseno on 10/30/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "InfoCell.h"

@implementation InfoCell

@synthesize titlelabel, codelabel, tradelabel, retaillabel, quantitylabel, productimage, quantityimage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *bgview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 140)];
        bgview.backgroundColor = [UIColor whiteColor];
        [self addSubview:bgview];
        
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 220, 40)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor grayColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:16];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.titlelabel = Title;
        [Title release];
        [self addSubview:titlelabel];
        
        UILabel *Code = [[UILabel alloc] initWithFrame:CGRectMake(90, 60, 170, 15)];
        Code.textAlignment = UITextAlignmentLeft;
        Code.textColor = [UIColor grayColor];
        Code.backgroundColor = [UIColor clearColor];
        Code.font = [UIFont systemFontOfSize:13];
        Code.lineBreakMode = UILineBreakModeWordWrap;
        Code.numberOfLines = 1;
        self.codelabel = Code;
        [Code release];
        [self addSubview:codelabel];
        
        UILabel *Trade = [[UILabel alloc] initWithFrame:CGRectMake(90, 75, 170, 15)];
        Trade.textAlignment = UITextAlignmentLeft;
        Trade.textColor = [UIColor grayColor];
        Trade.backgroundColor = [UIColor clearColor];
        Trade.font = [UIFont systemFontOfSize:13];
        Trade.lineBreakMode = UILineBreakModeWordWrap;
        Trade.numberOfLines = 1;
        self.tradelabel = Trade;
        [Trade release];
        [self addSubview:tradelabel];
        
        UILabel *Retail = [[UILabel alloc] initWithFrame:CGRectMake(90, 90, 170, 15)];
        Retail.textAlignment = UITextAlignmentLeft;
        Retail.textColor = [UIColor grayColor];
        Retail.backgroundColor = [UIColor clearColor];
        Retail.font = [UIFont systemFontOfSize:13];
        Retail.lineBreakMode = UILineBreakModeWordWrap;
        Retail.numberOfLines = 1;
        self.retaillabel = Retail;
        [Retail release];
        [self addSubview:retaillabel];
        
        UIImageView *ProductImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 6, 83, 104)];
        self.productimage = ProductImage;
        [ProductImage release];
        [self addSubview:productimage];
        
        UIImageView *QuantityImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"square.png"]];
        QuantityImage.frame = CGRectMake(20, 115, QuantityImage.image.size.width, QuantityImage.image.size.height);
        self.quantityimage = QuantityImage;
        [QuantityImage release];
        [self addSubview:quantityimage];
        
        UILabel *Quantity = [[UILabel alloc] initWithFrame:CGRectMake(50, 117, 40, 15)];
        Quantity.textAlignment = UITextAlignmentLeft;
        Quantity.textColor = [UIColor blackColor];
        Quantity.backgroundColor = [UIColor clearColor];
        Quantity.font = [UIFont systemFontOfSize:13];
        Quantity.lineBreakMode = UILineBreakModeWordWrap;
        Quantity.numberOfLines = 1;
        self.quantitylabel = Quantity;
        [Quantity release];
        [self addSubview:quantitylabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [titlelabel release];
    [codelabel release];
    [tradelabel release];
    [retaillabel release];
    [quantitylabel release];
    [productimage release];
    [quantityimage release];
    [super dealloc];
}

@end
