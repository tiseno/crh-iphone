//
//  InfoScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define InfoTableHeight 140
#define InfoTableWidth 300

#import "InfoScene.h"

@implementation InfoScene

@synthesize infotable, joblabel, totalcostview, tradelabel, retaillabel, editjobview, editjobinfobutton, titlelabel, backbutton, jobtitletextfield, priceview, typeview, jobrowselected, jobtitle, jobtitleview, topbar, bottombar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[UIScreen mainScreen] bounds].size.height > 480)
        self.topbar.frame = CGRectMake(0, 0, 320, 44);
    [self.topbar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"more_top_bar.png"]] autorelease] atIndex:1];
    [self.bottombar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"more_btm_bar.png"]] autorelease] atIndex:1];
    
    self.titlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
    
    self.joblabel.text = self.jobtitle;
    jobTitle = self.joblabel.text;
    
    db = [[DatabaseAction alloc] init];
    
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    productlist = [db retrieveProduct:myjobid];
    
    iseditmyjob = NO;
    
    if(productlist.count > 0)
    {
        totalretail = 0.00;
        totaltrade = 0.00;
        self.infotable.tableFooterView = self.totalcostview;
        self.totalcostview.backgroundColor = [UIColor colorWithRed:14.0/255.0 green:23.0/255.0 blue:32.0/255.0 alpha:1.0];
        for(int i = 0; i < productlist.count;i++)
        {
            Product *product = [productlist objectAtIndex:i];
            
            if(![product.producttrade isEqualToString:@"-"] || ![product.producttrade rangeOfString:@"Price on application"].location == NSNotFound)
            {
                NSString *trade = [product.producttrade substringWithRange:NSMakeRange(1, product.producttrade.length - 10)];
                trade = [trade stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totaltrade += ([trade floatValue] * product.productquantity);
            }
            self.tradelabel.text = [NSString stringWithFormat:@"%.2f", totaltrade];
            
            if(![product.productretail isEqualToString:@"-"]|| ![product.productretail rangeOfString:@"Price on application"].location == NSNotFound)
            {
                NSString *retail = [product.productretail substringWithRange:NSMakeRange(1, product.productretail.length - 10)];
                retail = [retail stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totalretail += ([retail floatValue] * product.productquantity);
            }
            self.retaillabel.text = [NSString stringWithFormat:@"%.2f", totalretail];
        }
    }
    else
    {
        self.infotable.tableFooterView = [[[UIView alloc] init] autorelease];
    }
    self.infotable.tableHeaderView = self.jobtitleview;
    
    self.totalcostview.backgroundColor = [UIColor colorWithRed:14.0/255.0 green:23.0/255.0 blue:32.0/255.0 alpha:1.0];
    
    [self.infotable reloadData];
    
    pricetransform = self.priceview.transform;
    self.priceview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"send_email_background_1.png"]];
    self.priceview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 20, [[UIScreen mainScreen] bounds].size.width, self.priceview.frame.size.height);
    [self.view addSubview:self.priceview];
    
    typetransform = self.typeview.transform;
    self.typeview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"send_email_background_2.png"]];
    self.typeview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 20, [[UIScreen mainScreen] bounds].size.width, self.typeview.frame.size.height);
    [self.view addSubview:self.typeview];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [infotable release];
    [joblabel release];
    [totalcostview release];
    [tradelabel release];
    [retaillabel release];
    [editjobview release];
    [editjobinfobutton release];
    [titlelabel release];
    [backbutton release];
    [jobtitletextfield release];
    [priceview release];
    [typeview release];
    [jobtitleview release];
    [topbar release];
    [bottombar release];
    
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productlist.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    infocell = (InfoCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(infocell == nil){
        infocell = [[[InfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    
    infocell.frame = CGRectMake(0, 0, InfoTableWidth, InfoTableHeight);
    infocell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    productlist = [db retrieveProduct:myjobid];
    Product *product = [productlist objectAtIndex:indexPath.row];
    
    infocell.titlelabel.text = product.producttitle;
    infocell.codelabel.text = [NSString stringWithFormat:@"CODE: %@", product.productcode];
    infocell.tradelabel.text = [NSString stringWithFormat:@"TRADE: %@", product.producttrade];
    infocell.retaillabel.text = [NSString stringWithFormat:@"RETAIL: %@", product.productretail];
    infocell.quantitylabel.text = [NSString stringWithFormat:@"x %d", product.productquantity];
    infocell.productimage.imageURL = [NSURL URLWithString:product.productimagepathS];
    
    UIButton *viewbutton = [[[UIButton alloc] initWithFrame:CGRectMake(256, 115, 49, 18)] autorelease];
    viewbutton.tag = indexPath.row;
    [viewbutton setBackgroundImage:[UIImage imageNamed:@"btn_view.png"] forState:UIControlStateNormal];
    [infocell addSubview:viewbutton];
    
    [viewbutton addTarget:self action:@selector(productdetailview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *deletebutton = [[[UIButton alloc] initWithFrame:CGRectMake(5 , (InfoTableHeight - 27) / 2 , 27, 27)] autorelease];
    int tag = [db retrieveProductselectedrow:indexPath.row :myjobid];
    deletebutton.tag = tag;
    
    if(iseditmyjob)
    {
        infocell.productimage.frame = CGRectMake(30, 0, 83, 104);
        infocell.titlelabel.frame = CGRectMake(120, 10, 180, 40);
        infocell.codelabel.frame = CGRectMake(120, 60, 180, 15);
        infocell.tradelabel.frame = CGRectMake(120, 75, 180, 15);
        infocell.retaillabel.frame = CGRectMake(120, 90, 180, 15);
        viewbutton.hidden = YES;
        
        [deletebutton setBackgroundImage:[UIImage imageNamed:@"btn_delete.png"] forState:UIControlStateNormal];
        [infocell addSubview:deletebutton];
        [deletebutton addTarget:self action:@selector(deletejobinfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        infocell.productimage.frame = CGRectMake(0, 0, 83, 104);
        infocell.titlelabel.frame = CGRectMake(90, 10, 210, 40);
        infocell.codelabel.frame = CGRectMake(90, 60, 210, 15);
        infocell.tradelabel.frame = CGRectMake(90, 75, 210, 15);
        infocell.retaillabel.frame = CGRectMake(90, 90, 210, 15);
        viewbutton.hidden = NO;
        [deletebutton removeFromSuperview];
    }
    
    return infocell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    infocell = (InfoCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return infocell.frame.size.height;
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)productdetailview:(id)sender
{
    ProductScene *GtestClasssViewController=[[[ProductScene alloc] initWithNibName:@"ProductScene"  bundle:nil] autorelease];
    
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    productlist = [db retrieveProduct:myjobid];
    Product *product = [productlist objectAtIndex:[sender tag]];
    
    GtestClasssViewController.appID = [NSString stringWithFormat:@"%d",product.sectionid];
    GtestClasssViewController.itemID = [NSString stringWithFormat:@"%d",product.categoryid];
    GtestClasssViewController.isjumpfromsearch = YES;
    
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)editjobinfo:(id)sender
{
    if(iseditmyjob)
    {
        NSString *check = [self.jobtitletextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![check isEqualToString:@""])
        {
            if([check length] > 32)
            {
                [self.view makeToast:@"Job name is too, please shorten it!" duration:(0.3) position:@"center"];
            }
            else
            {
                [self.editjobinfobutton setBackgroundImage:[UIImage imageNamed:@"btn_edit.png"] forState:UIControlStateNormal];
                int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
                [db updateMyjobtitle:myjobid :[self.jobtitletextfield.text stringByReplacingOccurrencesOfString:@"'" withString:@"\""]];
                self.joblabel.text = [db retrieveSelectedmyjobtitle:myjobid];
                self.infotable.tableHeaderView = self.jobtitleview;
                self.backbutton.enabled = YES;
                
                [jobtitletextfield resignFirstResponder];
                productlist = [db retrieveProduct:myjobid];
                [self.infotable reloadData];
                
                iseditmyjob = !iseditmyjob;
            }
        }
        else
        {
            [self.view makeToast:@"Job Name cannot be empty!" duration:(0.3) position:@"center"];
        }
    }
    else
    {
        [self.editjobinfobutton setBackgroundImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
        self.jobtitletextfield.text = [self.jobtitle stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
        self.infotable.tableHeaderView = self.editjobview;
        self.backbutton.enabled = NO;
        
        iseditmyjob = !iseditmyjob;
        
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        [self.infotable reloadData];
    }
}

-(IBAction)deletejobinfo:(id)sender
{
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    [db deleteProduct:[sender tag] :myjobid];
    productlist = [db retrieveProduct:myjobid];
    [self.infotable reloadData];
    if(productlist.count <= 0)
    {
        self.infotable.tableFooterView = [[[UIView alloc] init] autorelease];
    }
    else
    {
        totalretail = 0.00;
        totaltrade = 0.00;
        for(int i = 0; i < productlist.count;i++)
        {
            Product *product = [productlist objectAtIndex:i];
            
            if(![product.producttrade isEqualToString:@"-"] || ![product.producttrade rangeOfString:@"Price on application"].location == NSNotFound)
            {
                NSString *trade = [product.producttrade substringWithRange:NSMakeRange(1, product.producttrade.length - 10)];
                trade = [trade stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totaltrade += ([trade floatValue] * product.productquantity);
            }
            self.tradelabel.text = [NSString stringWithFormat:@"%.2f", totaltrade];
            
            if(![product.productretail isEqualToString:@"-"]|| ![product.productretail rangeOfString:@"Price on application"].location == NSNotFound)
            {
                NSString *retail = [product.productretail substringWithRange:NSMakeRange(1, product.productretail.length - 10)];
                retail = [retail stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totalretail += ([retail floatValue] * product.productquantity);
            }
            self.retaillabel.text = [NSString stringWithFormat:@"%.2f", totalretail];
        }
    }
}

-(IBAction)deletejob:(id)sender
{
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    [db deleteMyjob:myjobid];
    [db deleteProductWithJobid:myjobid];
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)email:(id)sender
{
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    productlist = [db retrieveProduct:myjobid];
    if(productlist.count > 0)
        [self priceviewanimationin];
}

-(IBAction)cancel:(id)sender
{
    if([sender tag] == 988)
    {
        [self priceviewanimationout];
    }
    else
    {
        [self typeviewanimationout];
        [self priceviewanimationin];
    }
}

-(IBAction)sendwithprice:(id)sender
{
    isneedprice = YES;
    [self priceviewanimationout];
    [self performSelector:@selector(typeviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)sendwithoutprice:(id)sender
{
    isneedprice = NO;
    [self priceviewanimationout];
    [self performSelector:@selector(typeviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)sendplain:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSMutableString *subject = [[NSMutableString alloc] init];
        NSMutableString *emailBody = [[NSMutableString alloc] init];
        
        
        [subject appendString:@"My Job - "];
        [subject appendString:jobTitle];
        
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        
        if(isneedprice)
        {
            for(int i = 0; i < productlist.count; i++)
            {
                Product *product = [productlist objectAtIndex:i];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@\n\n%@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle], [NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Trade: %@", product.producttrade], [NSString stringWithFormat:@"Retail: %@", product.productretail], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], product.productdescription];
                
                [emailBody appendString:appStr];
                
                if((i+1) == productlist.count)
                {
                    [emailBody appendString:@"\n\n\n"];
                }
            }
            [emailBody appendString:[NSString stringWithFormat:@"Total Trade Price : $ %.2f\n", totaltrade]];
            [emailBody appendString:[NSString stringWithFormat:@"Total Retail Price : $ %.2f", totalretail]];
            [emailBody appendString:@"\n\n\n"];
        }
        else
        {
            for(int i = 0; i < productlist.count; i++)
            {
                Product *product = [productlist objectAtIndex:i];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle], [NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], product.productdescription];
                
                [emailBody appendString:appStr];
                
                if((i+1) == productlist.count)
                {
                    [emailBody appendString:@"\n\n\n"];
                }
            }
        }
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:subject];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        [mailer setMessageBody:emailBody isHTML:NO];
        
        // only for iPad
        mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
        [subject release];
        [emailBody release];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
    
}

-(IBAction)sendhtml:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSMutableString *subject = [[NSMutableString alloc] init];
        NSMutableString *emailBody = [[NSMutableString alloc] init];
        
        
        [subject appendString:@"My Job - "];
        [subject appendString:jobTitle];
        
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        
        if(isneedprice)
        {
            for(int i = 0; i < productlist.count; i++)
            {
                Product *product = [productlist objectAtIndex:i];
                
                 NSString *filterNewLine = [NSString stringWithFormat:@"%@", [product.productdescription  stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@", product.productimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle],[NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Trade : %@", product.producttrade], [NSString stringWithFormat:@"Retail : %@", product.productretail], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], filterNewLine];
                
                [emailBody appendString:appStr];
                
                if((i+1) == productlist.count)
                {
                    [emailBody appendString:@"<br /><br /><br />"];
                }
            }
            [emailBody appendString:[NSString stringWithFormat:@"Total Trade Price : $ %.2f<br />", totaltrade]];
            [emailBody appendString:[NSString stringWithFormat:@"Total Retail Price : $ %.2f", totalretail]];
            [emailBody appendString:@"<br /><br /><br />"];
        }
        else
        {
            for(int i = 0; i < productlist.count; i++)
            {
                Product *product = [productlist objectAtIndex:i];
                
                NSString *filterNewLine = [NSString stringWithFormat:@"%@", [product.productdescription  stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@<br /><br />%@", product.productimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle], [NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], filterNewLine];
                
                [emailBody appendString:appStr];
                
                if((i+1) == productlist.count)
                {
                    [emailBody appendString:@"<br /><br /><br />"];
                }
            }
        }
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:subject];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        [mailer setMessageBody:emailBody isHTML:YES];
        
        // only for iPad
        mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
        [subject release];
        [emailBody release];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
}

-(IBAction)sendpdf:(id)sender
{
    [GlobalFunction AddLoadingScreen:self];
    [self.view makeToast:@"Generating PDF..." duration:(0.3) position:@"center"];
    [self performSelector:@selector(delayPDF) withObject:nil afterDelay:0.3];
}

-(void)delayPDF
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        NSString * timeStampValue = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
        
        NSMutableString *subject = [[NSMutableString alloc] init];
        
        NSMutableString *requestString = [[NSMutableString alloc] init];
        
        WebServices *ws = [[WebServices alloc] init];
        
        NSString *reply = [[NSString alloc] init];
        
        NSMutableString *emailBody = [[NSMutableString alloc] init];
        
        
        [subject appendString:@"My Job - "];
        [subject appendString:jobTitle];
        
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        
        if(isneedprice)
        {
            [requestString appendString:@"["];
            
            for(int i = 0; i < productlist.count; i++)
            {
                Product *product = [productlist objectAtIndex:i];
                
                NSString *filterStr = [product.productimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                
                NSString *filterDescStr = [product.productdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"µm" withString:@"micrometer"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"©" withString:@"(copyrighted)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
                
                NSString *filterTitleStr = [product.producttitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                
                NSString *productString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Trade\":\"%@\",\"Retail\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\",\"Quantity\":\"%d\",\"TotalTrade\":\"%@\",\"TotalRetail\":\"%@\"}", filterTitleStr, product.productcode, product.producttrade, product.productretail, filterDescStr, filterStr, timeStampValue, product.productquantity, [NSString stringWithFormat:@"%.2f", totaltrade], [NSString stringWithFormat:@"%.2f", totalretail]];
                
                [requestString appendString:productString];
                
                if((i+1) != productlist.count)
                {
                    [requestString appendString:@","];
                }
            }
            
            [requestString appendString:@"]"];
            
            reply = [ws generateListOfItemsPDFWithPrice:requestString];
            
            //add the final line here
            [emailBody appendString:@"<br />"];
            [emailBody appendString:[NSString stringWithFormat:@"Total Trade Price : $ %.2f<br />", totaltrade]];
            [emailBody appendString:[NSString stringWithFormat:@"Total Retail Price : $ %.2f<br /><br />", totalretail]];
        }
        else
        {
            [requestString appendString:@"["];
            
            for(int i = 0; i < productlist.count; i++)
            {
                Product *product = [productlist objectAtIndex:i];
                
                NSString *filterStr = [product.productimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                
                NSString *filterDescStr = [product.productdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"µm" withString:@"micrometer"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"©" withString:@"(copyrighted)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
                
                NSString *filterTitleStr = [product.producttitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                
                NSString *productString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\",\"Quantity\":\"%d\"}", filterTitleStr, product.productcode, filterDescStr, filterStr, timeStampValue, product.productquantity];
                
                [requestString appendString:productString];
                
                if((i+1) != productlist.count)
                {
                    [requestString appendString:@","];
                }
            }
            
            [requestString appendString:@"]"];
            
            reply = [ws generateListOfItemsPDFWithoutPrice:requestString];
        }
        
        
        if([reply isEqualToString:@"Success"])
        {
            NSString *pdf_link = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/tiseno/fpdf17/product_pdf/%@.pdf", timeStampValue];
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:subject];
            
            [emailBody appendString:[NSString stringWithFormat:@"<br /><br /><br /><br />Click the link to download the pdf file : <a href=\"%@\">%@.pdf</a>", pdf_link, timeStampValue]];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            [mailer setMessageBody:emailBody isHTML:YES];
            
            // only for iPad
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
            [subject release];
            [ws release];
            [reply release];
            [emailBody release];
        }
        else
        {
            [self.view makeToast:@"Failed to generate PDF..." duration:(0.3) position:@"center"];
        }
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
    
    [GlobalFunction RemoveLoadingScreen:self];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)priceviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -self.priceview.frame.size.height);
        self.priceview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             pricetransform = self.priceview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)priceviewanimationout
{
    self.priceview.transform = pricetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, self.priceview.frame.size.height);
        self.priceview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             pricetransform = self.priceview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)typeviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -self.typeview.frame.size.height);
        self.typeview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             typetransform = self.typeview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)typeviewanimationout
{
    self.typeview.transform = typetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, self.typeview.frame.size.height);
        self.typeview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             typetransform = self.typeview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

@end
