//
//  ProductScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//
#define ProductTableHeight 105
#define MainTableWidth 320
#define JobTableWidth 290

#import "ProductScene.h"

@implementation ProductScene

@synthesize headerview, categoryheaderbg, producttable, categorylabel;

@synthesize categorytext, itemArr, totalItem, itemPriceArr, middleImageArr, tradeArr, row, rowselected, isjump, isjumpfromsearch, appID, itemID;

@synthesize detailscrollview, productimage, buttonbgimage, titlelabel, codelabel, tradelabel, retaillabel, detailview, detailbgimage, seriallabel, titlebgimage, detailshadowimage;

@synthesize galleryImageArr, technicalImageArr, descriptiontext;

@synthesize popupview, popupimage, popuplabel, popuppreviousbutton, popupnextbutton, poptitlelabel, popupclosebutton, loadingview;

@synthesize jobtitlelabel, jobcodelabel, jobfixlabel, jobproductimage, jobtable, jobview, jobshadowimage;

@synthesize enquiryview, popfirstnametextfield, poplastnametextfield, popemailtextfield, popaddresstextfield, popcompanytextfield, popphonetextfield, popproductcodetextfield, popcommentstextview, popscrollview, popshadowimage, popnotselectedbutton, popselectedbutton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    transparentview = [[[UIView alloc]init] autorelease];
    transparentview.backgroundColor = [UIColor clearColor];
    
    self.producttable.backgroundColor = [UIColor blackColor];
    self.producttable.tableFooterView = [[[UIView alloc] init] autorelease];
    
    self.categorylabel.text = self.categorytext;
    
    detailtransform = self.detailview.transform;
    self.detailview.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width, self.producttable.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, self.producttable.frame.size.height);
    self.productimage.frame = CGRectMake(67, 0, 216, 110);
    [self.view addSubview:self.detailview];
    
    jobtransform = self.jobview.transform;
    self.jobview.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width, self.producttable.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, self.producttable.frame.size.height);
    self.jobview.backgroundColor = [UIColor clearColor];
    self.jobtable.tableFooterView = transparentview;
    self.jobproductimage.frame = CGRectMake(48, 10, 60, 75);
    
    [self.view addSubview:self.jobview];
    
    enquirytransform = self.enquiryview.transform;
    self.poptitlelabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"enquiry_top_bar.png"]];
    self.enquiryview.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width, self.producttable.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, self.producttable.frame.size.height);
    self.enquiryview.backgroundColor = [UIColor clearColor];
    self.popscrollview.frame = CGRectMake(30, 44, 290, 330);
    self.popscrollview.contentSize = CGSizeMake(290, 570);
    [self.view addSubview:self.enquiryview];
    
    pricetransform = self.priceview.transform;
    self.priceview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"send_email_background_1.png"]];
    self.priceview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 20, [[UIScreen mainScreen] bounds].size.width, self.priceview.frame.size.height);
    [self.view addSubview:self.priceview];
    
    typetransform = self.typeview.transform;
    self.typeview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"send_email_background_2.png"]];
    self.typeview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 20, [[UIScreen mainScreen] bounds].size.width, self.typeview.frame.size.height);
    [self.view addSubview:self.typeview];
    
    numberlayer = 1;
    
    issubcribe = YES;
    self.popselectedbutton.enabled = NO;
    
    db = [[DatabaseAction alloc] init];
    
    if(self.isjump)
    {
        [GlobalFunction AddLoadingScreen:self];
        self.view.hidden = YES;
        productrowselected = self.rowselected;
        [self performSelector:@selector(delay1) withObject:nil afterDelay:0.05];
    }
    
    if(self.isjumpfromsearch)
    {
        [GlobalFunction AddLoadingScreen:self];
        self.headerview.hidden = YES;
        self.producttable.hidden = YES;
        [self performSelector:@selector(delay4) withObject:nil afterDelay:1];
    }
    else
    {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        red = [[appDelegate.colorcodered objectAtIndex:row] floatValue];
        green = [[appDelegate.colorcodegreen objectAtIndex:row] floatValue];
        blue = [[appDelegate.colorcodeblue objectAtIndex:row] floatValue];
        
        self.categoryheaderbg.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.categoryheaderbg.frame;
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1] CGColor], [appDelegate.gradientbottom objectAtIndex:row], nil];
        [self.categoryheaderbg.layer insertSublayer:gradient atIndex:0];
        
        self.jobfixlabel.textColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
        
        self.detailshadowimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"border_%d.png", row+1]];
        self.jobshadowimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"border_%d.png", row+1]];
        self.popshadowimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"border_%d.png", row+1]];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
    
    [[popcommentstextview layer] setBorderWidth:1];
    [[popcommentstextview layer] setCornerRadius:5];
    
    popfirstnametextfield.enablesReturnKeyAutomatically = NO;
    poplastnametextfield.enablesReturnKeyAutomatically = NO;
    popemailtextfield.enablesReturnKeyAutomatically = NO;
    popaddresstextfield.enablesReturnKeyAutomatically = NO;
    popcompanytextfield.enablesReturnKeyAutomatically = NO;
    popphonetextfield.enablesReturnKeyAutomatically = NO;
    popcommentstextview.enablesReturnKeyAutomatically = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [headerview release];
    [categoryheaderbg release];
    [producttable release];
    [categorylabel release];
    
    [productimage release];
    [buttonbgimage release];
    [detailscrollview release];
    [titlelabel release];
    [codelabel release];
    [tradelabel release];
    [retaillabel release];
    [detailview release];
    [detailbgimage release];
    [seriallabel release];
    [titlebgimage release];
    [detailshadowimage release];
    
    [popupview release];
    [popupimage release];
    [popuplabel release];
    [popuppreviousbutton release];
    [popupnextbutton release];
    [popupclosebutton release];
    [loadingview release];
    
    [jobtitlelabel release];
    [jobcodelabel release];
    [jobfixlabel release];
    [jobproductimage release];
    [jobtable release];
    [jobview release];
    [jobshadowimage release];
    
    [enquiryview release];
    [popfirstnametextfield release];
    [poplastnametextfield release];
    [popemailtextfield release];
    [popaddresstextfield release];
    [popcompanytextfield release];
    [popphonetextfield release];
    [popproductcodetextfield release];
    [popcommentstextview release];
    [poptitlelabel release];
    [popscrollview release];
    [popshadowimage release];
    [popnotselectedbutton release];
    [popselectedbutton release];
    
    [alert release];
    [db release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == producttable)
    {
        return self.itemArr.count;
    }
    else
    {
        return joblist.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.producttable)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        productcell= (ProductCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(productcell == nil)
        {
            productcell = [[[ProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        productcell.frame = CGRectMake(0, 0, MainTableWidth, ProductTableHeight);
        productcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(itemArr != nil || [itemArr count] != 0)
        {
            NSError* error;
            
            NSArray* itemInfo = [totalItem objectAtIndex:indexPath.row];
            
            NSDictionary* singleItem = [itemInfo objectAtIndex:0];
            NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
            
            NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the description of the product
            NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
            NSDictionary *item_ = [itemDescription objectForKey:@"0"];
            
            NSString *filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
            
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
            
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
            
            BOOL happen = true;
            
            do
            {
                NSRange strBegin = [filterStr rangeOfString:@"<span"];
                
                if(strBegin.location == NSNotFound)
                {
                    happen = false;
                }
                else
                {
                    NSRange strEnd = [filterStr rangeOfString:@"/>"];
                    
                    if(strEnd.location == NSNotFound)
                    {
                        //nothing here
                        happen = false;
                    }
                    else
                    {
                        //do the operation
                        filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
                    }
                }
            } while (happen);
            
            productcell.backgroundview.backgroundColor = [UIColor whiteColor];
            
            productcell.title.text = [singleDetails objectForKey:@"name"];
            
            productcell.description.text = filterStr;
            
            NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            productcell.code.text = [NSString stringWithFormat:@"Code : %@",[itemCode objectForKey:@"value"]];
            
            productcell.serial.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
            
            NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            productcell.productimage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
        }
        
        productcell.serialimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"cat_tri_%d.png", self.row + 1]];
        
        return productcell;
    }
    else if(tableView == self.jobtable)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        jobcell= (JobCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(jobcell == nil){
            jobcell = [[[JobCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        jobcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int myjobid = [db retrieveMyjobselectedrow:indexPath.row];
        joblist = [db retrieveMyjob];
        Myjob *myjob = [joblist objectAtIndex:indexPath.row];
        jobcell.jobtitle.text = [myjob.myjobtitle stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];;
        
        CGSize maxsize = CGSizeMake(200, 9999);
        UIFont *thefont = [UIFont boldSystemFontOfSize:13];
        CGSize textsize = [jobcell.jobtitle.text sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        jobcell.frame = CGRectMake(0, 0, JobTableWidth, 25 + textsize.height);
        
        jobcell.jobtitle.frame = CGRectMake(10, 5, 200, textsize.height);
        jobcell.jobdescription.frame = CGRectMake(10, jobcell.jobtitle.frame.origin.y + jobcell.jobtitle.frame.size.height + 3, 200, 12);
        
        UIButton *addproductbutton = [[[UIButton alloc] initWithFrame:CGRectMake(JobTableWidth - 70, (jobcell.frame.size.height -21) / 2, 21, 21)] autorelease];
        addproductbutton.tag = indexPath.row;
        [addproductbutton setBackgroundImage:[UIImage imageNamed:@"btn_add.png"] forState:UIControlStateNormal];
        [jobcell addSubview:addproductbutton];
        [addproductbutton addTarget:self action:@selector(addproductinjob:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *deleteproductbutton = [[[UIButton alloc] initWithFrame:CGRectMake(JobTableWidth - 100, (jobcell.frame.size.height -21) / 2, 21, 21)] autorelease];
        deleteproductbutton.tag = indexPath.row;
        [deleteproductbutton setBackgroundImage:[UIImage imageNamed:@"btn_remove.png"] forState:UIControlStateNormal];
        [jobcell addSubview:deleteproductbutton];
        deleteproductbutton.enabled = NO;
        [deleteproductbutton addTarget:self action:@selector(removeproductinjob:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *quantitybutton = [[[UIButton alloc] initWithFrame:CGRectMake(JobTableWidth - 40, 9, 32, 23)] autorelease];
        quantitybutton.tag = indexPath.row;
        [quantitybutton setBackgroundImage:[UIImage imageNamed:@"btn_quantity.png"] forState:UIControlStateNormal];
        [jobcell addSubview:quantitybutton];
        [quantitybutton addTarget:self action:@selector(quantityadd:) forControlEvents:UIControlEventTouchUpInside];
        
        if([db countProductinjob:myjobid :[self.codelabel.text substringFromIndex:7]]> 0)
        {
            jobcell.jobdescription.text = [NSString stringWithFormat:@"Has %d of These", [db retrieveProductQuantity:myjobid :[self.codelabel.text substringFromIndex:7]]];
            
            if([db retrieveProductQuantity:myjobid :[self.codelabel.text substringFromIndex:7]] > 0)
            {
                deleteproductbutton.enabled = YES;
            }
        }
        else
        {
            deleteproductbutton.enabled = NO;
            jobcell.jobdescription.text = [NSString stringWithFormat:@" Has 0 of Those"];
        }
        
        return jobcell;
    }
    else
    {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    productcell= (ProductCell*)[tableView cellForRowAtIndexPath:indexPath];
    productrowselected = indexPath.row;
    [GlobalFunction AddLoadingScreen:self];
    [self performSelector:@selector(delay1) withObject:nil afterDelay:0.1];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    if(tableView == producttable)
    {
        productcell= (ProductCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return productcell.frame.size.height;
    }
    else
    {
        jobcell= (JobCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return jobcell.frame.size.height;
    }
}

-(void)delay1
{
    NSError* error;
    
    NSArray* itemInfo;
    
    if(self.isjump)
    {
        itemInfo = [totalItem objectAtIndex:self.rowselected];
        [self.producttable selectRowAtIndexPath:[NSIndexPath indexPathForRow:productrowselected inSection:self.producttable.indexPathForSelectedRow.section] animated:NO scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        itemInfo = [totalItem objectAtIndex:productrowselected];
    }
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    titleOfRowSelected = [singleDetails objectForKey:@"name"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    //Get imasge path - 1
    NSDictionary *galleryKey = [itemDetails objectForKey:@"5f304277-9eff-4033-88b9-e231db615697"];
    
    //Get image path - 2
    NSDictionary *technicalKey = [itemDetails objectForKey:@"35edcd61-decc-4ba2-9146-995bcbcb0245"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    galleryImageArr = [[ws selectGalleryImageByPath:[galleryKey objectForKey:@"value"]] retain];
    technicalImageArr = [[ws selectTechnicalImageByPath:[technicalKey objectForKey:@"value"]] retain];
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    NSString *filterStr, *codeStr;
    
    NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
    NSDictionary *item_ = [itemDescription objectForKey:@"0"];
    
    NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
    NSDictionary *itemCode = [itemDic objectForKey:@"0"];
    
    codeStr = [itemCode objectForKey:@"value"];
    
    filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
    
    BOOL happen = true;
    
    do
    {
        NSRange strBegin = [filterStr rangeOfString:@"<span"];
        
        if(strBegin.location == NSNotFound)
        {
            happen = false;
        }
        else
        {
            NSRange strEnd = [filterStr rangeOfString:@"/>"];
            
            if(strEnd.location == NSNotFound)
            {
                //nothing here
                happen = false;
            }
            else
            {
                //do the operation
                filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
            }
        }
    } while (happen);
    
    self.descriptiontext = filterStr;
    
    //detailview setting
    self.detailview.backgroundColor = [UIColor clearColor];
    self.buttonbgimage.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    self.detailbgimage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    CGSize titlesize = CGSizeMake(260, 9999);
    UIFont *titlefont = [UIFont boldSystemFontOfSize:13];
    CGSize titletextsize = [[NSString stringWithFormat:@"%@", titleOfRowSelected] sizeWithFont:titlefont constrainedToSize:titlesize lineBreakMode:UILineBreakModeWordWrap];
    
    self.titlebgimage.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    self.titlebgimage.frame = CGRectMake(30, 147, 290, titletextsize.height + 15);
    
    self.titlelabel.frame = CGRectMake(60, 147, 250, titletextsize.height + 15);
    if(titletextsize.height > 20)
        self.seriallabel.frame = CGRectMake(30, 147, 30, titletextsize.height / 2 + 15);
    else
        self.seriallabel.frame = CGRectMake(30, 147, 30, titletextsize.height + 15);
    
    self.detailscrollview.frame = CGRectMake(30, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height, 290, 327-(self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height));
    self.detailbgimage.frame = CGRectMake(30, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height, 290, 416-(self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height));
    
    self.titlelabel.text = [NSString stringWithFormat:@"%@", titleOfRowSelected];
    self.seriallabel.text = [NSString stringWithFormat:@"%d) ", productrowselected + 1];
    
    self.codelabel.text = [NSString stringWithFormat:@"Code : %@", codeStr];
    
    productCodeOfRowSelected = self.codelabel.text;
    
    NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *test2 = [test objectForKey:@"695755a4-ae67-43bb-8c39-af4eb9ad4961"];
    NSDictionary *test3 = [test2 objectForKey:@"option"];
    
    NSDictionary *test5 = [[[NSDictionary alloc] init] autorelease];
    
    // trade price INCL GST part
    if([test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"] != nil)
    {
        NSDictionary *test4 = [test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"];
        
        test5 = [test4 objectForKey:@"option"];
    }
    
    if([tradeArr objectAtIndex:productrowselected] != nil && ![[tradeArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if(![[tradeArr objectAtIndex:productrowselected] isEqualToString:@"Price on application"])
        {
            if([test5 objectForKey:@"0"] != nil && ![[test5 objectForKey:@"0"] isEqualToString:@""])
                self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Incl GST", [tradeArr objectAtIndex:productrowselected]];
            else
                self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Excl GST", [tradeArr objectAtIndex:productrowselected]];
        }
        else
        {
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : %@", [tradeArr objectAtIndex:productrowselected]];
        }
    }
    else
        self.tradelabel.text = @"Trade : -";
    
    if([itemPriceArr objectAtIndex:productrowselected] != nil && ![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if(![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@"Price on application"])
        {
            if([test3 objectForKey:@"0"] != nil && ![[test3 objectForKey:@"0"] isEqualToString:@""])
                self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Incl GST", [itemPriceArr objectAtIndex:productrowselected]];
            else
                self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Excl GST", [itemPriceArr objectAtIndex:productrowselected]];
        }
        else
        {
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : %@", [itemPriceArr objectAtIndex:productrowselected]];
        }
    }
    else
    {
        self.retaillabel.text = @"Retail : -";
    }
    
    imagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    self.productimage.imageURL = [NSURL URLWithString:imagepathM];
    
    self.productimage.backgroundColor = [UIColor whiteColor];
    
    descriptionlabel.text = @"";
    CGSize maxsize = CGSizeMake(270, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:11];
    CGSize textsize = [filterStr sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    descriptionlabel = [[[UILabel alloc]initWithFrame:CGRectMake(0, 85, 384, textsize.height)] autorelease];
    descriptionlabel.tag = 999;
    descriptionlabel.textColor = [UIColor blackColor];
    descriptionlabel.font = [UIFont fontWithName:@"Helvetica" size:11];
    descriptionlabel.text = filterStr;
    descriptionlabel.numberOfLines = 0;
    descriptionlabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionlabel.backgroundColor = [UIColor clearColor];
    [self.detailscrollview addSubview:descriptionlabel];
    self.detailscrollview.contentSize = CGSizeMake(290, 60 + descriptionlabel.frame.size.height + 5);
    
    self.view.hidden = NO;
    
    [self detailviewanimationin];
}

-(void)delay4
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    //first, get category id
    NSArray *categoryList2 = [ws selectCategoryIDByID:[self.itemID intValue]];
    NSString *categoryID = @"0";
    
    for(int i = 0; i < categoryList2.count; i++)
    {
        NSDictionary* catInfo2 = [categoryList2 objectAtIndex:i];
        NSDictionary* catDetails = [catInfo2 objectForKey:@"cat"];
        
        categoryID = [catDetails objectForKey:@"category_id"];
    }
    
    NSArray *parentKeyArr = [ws getParentKeyByItemID:categoryID];
    
    for(int i = 0; i < parentKeyArr.count; i++)
    {
        NSDictionary* novainfo = [parentKeyArr objectAtIndex:i];
        
        NSDictionary *novadetails = [novainfo objectForKey:@"parentid"];
        
        appID = [NSString stringWithFormat:@"%@", [novadetails objectForKey:@"parent"]];
        
        NSLog(@"app id ... %@", appID);
    }
    
    NSArray *mainArr = [[ws selectAllFromApplicationTableExceptParam] retain];
    for(int i = 0; i < mainArr.count; i++)
    {
        NSDictionary* novainfo = [mainArr objectAtIndex:i];
        NSDictionary* novadetails = [novainfo objectForKey:@"application"];
        
        if([[novadetails objectForKey:@"id"]isEqualToString:self.appID])
        {
            row = i;
            
            NSLog(@"color code ... %d", row);   // here gt the colorIndex
        }
    }
    
    NSArray *appName = [ws selectApplicationNameByID:[appID intValue]];
    NSDictionary* appInfo = [appName objectAtIndex:0];
    NSDictionary* appDetails = [appInfo objectForKey:@"app"];
    
    NSLog(@"app name ... %@", [appDetails objectForKey:@"name"]);
    
    self.categorylabel.text = [appDetails objectForKey:@"name"];
    
    NSArray *categoryList = [ws selectAllFromCategoryTableExceptParam:[self.appID intValue]];
    
    for(int i = 0; i < categoryList.count; i++)
    {
        NSDictionary* catInfo = [categoryList objectAtIndex:i];
        NSDictionary* catDetails = [catInfo objectForKey:@"category"];
        
        if([[catDetails objectForKey:@"id"] intValue] == [categoryID intValue])
        {
            rowselected = i;
        }
    }
    
    NSArray *itemList = [ws selectAllCategoryItem:[itemID intValue]];
    self.itemArr = itemList;
    self.totalItem = [[[NSMutableArray alloc] initWithCapacity:self.itemArr.count+1] retain];
    
    for(int i = 0; i < itemList.count; i++)
    {
        NSDictionary* itemInfo = [itemList objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [self.totalItem addObject:item];
        
        if([[infoDetails objectForKey:@"item_id"] intValue] == [itemID intValue])
        {
            productrowselected = i;
        }
    }
    
    self.itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:self.totalItem.count+1] retain];
    self.middleImageArr = [[[NSMutableArray alloc] initWithCapacity:self.totalItem.count+1] retain];
    self.tradeArr = [[[NSMutableArray alloc] initWithCapacity:self.totalItem.count+1] retain];
    
    for(int j = 0; j < self.totalItem.count; j++)
    {
        NSArray* itemInfo = [self.totalItem objectAtIndex:j];
        
        NSDictionary* items = [itemInfo objectAtIndex:0];
        NSDictionary* infoDetails = [items objectForKey:@"item"];
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *middleimg = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        NSDictionary *tradeKey = [elements objectForKey:@"953ff3d0-753c-47f7-aeec-96599ecc4814"];
        NSDictionary *trade = [tradeKey objectForKey:@"0"];
        
        [self.middleImageArr addObject:[middleimg objectForKey:@"file"]];
        [self.itemPriceArr addObject:[price objectForKey:@"value"]];
        [self.tradeArr addObject:[trade objectForKey:@"value"]];
    }
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    titleOfRowSelected = [singleDetails objectForKey:@"name"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    //Get imasge path - 1
    NSDictionary *galleryKey = [itemDetails objectForKey:@"5f304277-9eff-4033-88b9-e231db615697"];
    
    //Get image path - 2
    NSDictionary *technicalKey = [itemDetails objectForKey:@"35edcd61-decc-4ba2-9146-995bcbcb0245"];
    
    galleryImageArr = [[ws selectGalleryImageByPath:[galleryKey objectForKey:@"value"]] retain];
    technicalImageArr = [[ws selectTechnicalImageByPath:[technicalKey objectForKey:@"value"]] retain];
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    NSString *filterStr, *codeStr;
    
    NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
    NSDictionary *item_ = [itemDescription objectForKey:@"0"];
    
    NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
    NSDictionary *itemCode = [itemDic objectForKey:@"0"];
    
    codeStr = [itemCode objectForKey:@"value"];
    
    filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
    
    BOOL happen = true;
    
    do
    {
        NSRange strBegin = [filterStr rangeOfString:@"<span"];
        
        if(strBegin.location == NSNotFound)
        {
            happen = false;
        }
        else
        {
            NSRange strEnd = [filterStr rangeOfString:@"/>"];
            
            if(strEnd.location == NSNotFound)
            {
                //nothing here
                happen = false;
            }
            else
            {
                //do the operation
                filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
            }
        }
    } while (happen);
    
    self.descriptiontext = filterStr;
    
    //detailview setting
    self.detailview.backgroundColor = [UIColor clearColor];
    self.buttonbgimage.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    self.detailbgimage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    CGSize titlesize = CGSizeMake(260, 9999);
    UIFont *titlefont = [UIFont boldSystemFontOfSize:13];
    CGSize titletextsize = [[NSString stringWithFormat:@"%@", titleOfRowSelected] sizeWithFont:titlefont constrainedToSize:titlesize lineBreakMode:UILineBreakModeWordWrap];
    
    self.titlebgimage.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    self.titlebgimage.frame = CGRectMake(30, 147, 290, titletextsize.height + 15);
    
    self.titlelabel.frame = CGRectMake(60, 147, 250, titletextsize.height + 15);
    if(titletextsize.height > 20)
        self.seriallabel.frame = CGRectMake(30, 147, 30, titletextsize.height / 2 + 15);
    else
        self.seriallabel.frame = CGRectMake(30, 147, 30, titletextsize.height + 15);
    
    self.detailscrollview.frame = CGRectMake(30, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height, 290, 327-(self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height));
    self.detailbgimage.frame = CGRectMake(30, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height, 290, 416-(self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height));
    
    self.titlelabel.text = [NSString stringWithFormat:@"%@", titleOfRowSelected];
    self.seriallabel.text = [NSString stringWithFormat:@"%d) ", productrowselected + 1];
    
    self.codelabel.text = [NSString stringWithFormat:@"Code : %@", codeStr];
    
    productCodeOfRowSelected = self.codelabel.text;
    
    NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *test2 = [test objectForKey:@"695755a4-ae67-43bb-8c39-af4eb9ad4961"];
    NSDictionary *test3 = [test2 objectForKey:@"option"];
    
    NSDictionary *test5 = [[[NSDictionary alloc] init] autorelease];
    
    // trade price INCL GST part
    if([test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"] != nil)
    {
        NSDictionary *test4 = [test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"];
        
        test5 = [test4 objectForKey:@"option"];
    }
    
    if([tradeArr objectAtIndex:productrowselected] != nil && ![[tradeArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if(![[tradeArr objectAtIndex:productrowselected] isEqualToString:@"Price on application"])
        {
            if([test5 objectForKey:@"0"] != nil && ![[test5 objectForKey:@"0"] isEqualToString:@""])
                self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Incl GST", [tradeArr objectAtIndex:productrowselected]];
            else
                self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Excl GST", [tradeArr objectAtIndex:productrowselected]];
        }
        else
        {
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : %@", [tradeArr objectAtIndex:productrowselected]];
        }
    }
    else
        self.tradelabel.text = @"Trade : -";
    
    if([itemPriceArr objectAtIndex:productrowselected] != nil && ![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if(![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@"Price on application"])
        {
            if([test3 objectForKey:@"0"] != nil && ![[test3 objectForKey:@"0"] isEqualToString:@""])
                self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Incl GST", [itemPriceArr objectAtIndex:productrowselected]];
            else
                self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Excl GST", [itemPriceArr objectAtIndex:productrowselected]];
        }
        else
        {
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : %@", [itemPriceArr objectAtIndex:productrowselected]];
        }
    }
    else
    {
        self.retaillabel.text = @"Retail : -";
    }
    
    imagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    self.productimage.imageURL = [NSURL URLWithString:imagepathM];
    
    self.productimage.backgroundColor = [UIColor whiteColor];
    
    descriptionlabel.text = @"";
    CGSize maxsize = CGSizeMake(270, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:11];
    CGSize textsize = [filterStr sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    descriptionlabel = [[[UILabel alloc]initWithFrame:CGRectMake(0, 85, 384, textsize.height)] autorelease];
    descriptionlabel.tag = 999;
    descriptionlabel.textColor = [UIColor blackColor];
    descriptionlabel.font = [UIFont fontWithName:@"Helvetica" size:11];
    descriptionlabel.text = filterStr;
    descriptionlabel.numberOfLines = 0;
    descriptionlabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionlabel.backgroundColor = [UIColor clearColor];
    [self.detailscrollview addSubview:descriptionlabel];
    self.detailscrollview.contentSize = CGSizeMake(290, 60 + descriptionlabel.frame.size.height + 5);
    
    self.headerview.hidden = NO;
    self.producttable.hidden = NO;
    [self.producttable reloadData];
    
    [self.producttable selectRowAtIndexPath:[NSIndexPath indexPathForRow:productrowselected inSection:self.producttable.indexPathForSelectedRow.section] animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    red = [[appDelegate.colorcodered objectAtIndex:row] floatValue];
    green = [[appDelegate.colorcodegreen objectAtIndex:row] floatValue];
    blue = [[appDelegate.colorcodeblue objectAtIndex:row] floatValue];
    
    self.categoryheaderbg.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.categoryheaderbg.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1] CGColor], [appDelegate.gradientbottom objectAtIndex:row], nil];
    [self.categoryheaderbg.layer insertSublayer:gradient atIndex:0];
    
    self.jobfixlabel.textColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    
    self.detailshadowimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"border_%d.png", row+1]];
    self.jobshadowimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"border_%d.png", row+1]];
    self.popshadowimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"border_%d.png", row+1]];
    
    [self detailviewanimationin];
}

-(void)detailviewanimationin
{
    self.detailview.backgroundColor = [UIColor clearColor];
    self.buttonbgimage.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    self.detailbgimage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    CGSize titlesize = CGSizeMake(260, 9999);
    UIFont *titlefont = [UIFont boldSystemFontOfSize:13];
    CGSize titletextsize = [[NSString stringWithFormat:@"%@", titleOfRowSelected] sizeWithFont:titlefont constrainedToSize:titlesize lineBreakMode:UILineBreakModeWordWrap];
    
    self.titlebgimage.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    self.titlebgimage.frame = CGRectMake(30, 147, 290, titletextsize.height + 15);
    
    self.titlelabel.frame = CGRectMake(60, 147, 250, titletextsize.height + 15);
    if(titletextsize.height > 20)
        self.seriallabel.frame = CGRectMake(30, 147, 30, titletextsize.height / 2 + 15);
    else
        self.seriallabel.frame = CGRectMake(30, 147, 30, titletextsize.height + 15);
    self.detailscrollview.frame = CGRectMake(30, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height, 290, 327-(self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height));
    self.detailbgimage.frame = CGRectMake(30, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height, 290, 416-(self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height));
    
    CGSize maxsize = CGSizeMake(270, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:11];
    CGSize textsize = [self.descriptiontext sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    descriptionlabel.frame = CGRectMake(10, 60, 270, textsize.height + 5);
    self.detailscrollview.contentSize = CGSizeMake(290, 60 + descriptionlabel.frame.size.height + 5);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(-[[UIScreen mainScreen] bounds].size.width, 0);
        self.detailview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             detailtransform = self.detailview.transform;
                             numberlayer = 2;
                             
                             
                         }
                     }];
    [UIView commitAnimations];
}

-(void)detailviewanimationout
{
    self.detailview.transform = detailtransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation([[UIScreen mainScreen] bounds].size.width, 0);
        self.detailview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             detailtransform = self.detailview.transform;
                             numberlayer = 1;
                         }
                     }];
    [UIView commitAnimations];
}

-(IBAction)back:(id)sender
{
    if(numberlayer == 1)
        [super dismissModalViewControllerAnimated:YES];
    else if(numberlayer == 2)
    {
        self.isjump = NO;
        [self detailviewanimationout];
    }
}

-(IBAction)zoomin:(id)sender
{
    [self.popupview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    istechnical = NO;
    
    if(galleryImageArr.count > 0)
    {
        self.popupimage.imageURL = nil;
        NSDictionary* galleryImage = [galleryImageArr objectAtIndex:0];
        
        NSString *imageUrl = [[galleryImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
        inviewpage = 0;
        self.popuplabel.text = [NSString stringWithFormat:@"\t\t%@", titleOfRowSelected];
        checkimagetimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkimage) userInfo:nil repeats:YES];
        [self.view addSubview:self.popupview];
    }
    else
    {
        [self.view makeToast:@"No images to view！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)drawing:(id)sender
{
    [self.popupview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    istechnical = YES;
    
    if(technicalImageArr.count > 0)
    {
        self.popupimage.imageURL = nil;
        NSDictionary* technicalImage = [technicalImageArr objectAtIndex:0];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
        inviewpage = 0;
        self.popuplabel.text = [NSString stringWithFormat:@"\t\t%@", titleOfRowSelected];
        checkimagetimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkimage) userInfo:nil repeats:YES];
        [self.view addSubview:self.popupview];
    }
    else
    {
        [self.view makeToast:@"No drawing to view！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)bookmarks:(id)sender
{
    NSError* error;
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
    
    Bookmark *bookmark = [[Bookmark alloc] init];
    bookmark.bookmarktitle = titlelabel.text;
    bookmark.bookmarkcode = [codelabel.text substringFromIndex:7];
    bookmark.bookmarktrade = [tradelabel.text substringFromIndex:8];
    bookmark.bookmarkretail = [retaillabel.text substringFromIndex:9];
    descriptionlabel.text = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"'" withString:@""];
    bookmark.bookmarkdescription = descriptionlabel.text;
    bookmark.bookmarkimagepathS = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]];
    bookmark.bookmarkimagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    bookmark.sectionid = [[singleDetails objectForKey:@"application_id"] intValue];
    bookmark.categoryid = [[singleDetails objectForKey:@"id"] intValue];
    [db insertBookmark:bookmark];
    [bookmark release];
    
    alert = [[UIAlertView alloc] initWithTitle:nil message:@"Successfully added product into bookmark！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)add:(id)sender
{
    NSError* error;
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
    
    self.jobproductimage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
    
    NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
    NSDictionary *itemCode = [itemDic objectForKey:@"0"];
    
    self.jobtitlelabel.text = [singleDetails objectForKey:@"name"];
    
    self.jobcodelabel.text = [itemCode objectForKey:@"value"];
    
    [self detailviewanimationout];
    [self performSelector:@selector(addjobviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)enquiry:(id)sender
{
    popproductcodetextfield.text = [productCodeOfRowSelected substringFromIndex:7];
    [self detailviewanimationout];
    [self performSelector:@selector(enquiryviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)email:(id)sender
{
    [self detailviewanimationout];
    [self performSelector:@selector(priceviewanimationin) withObject:nil afterDelay:0.4];
}

//galleryview
-(void)checkimage
{
    if(self.popupimage.image == nil)
    {
        self.popupnextbutton.hidden = YES;
        self.popuppreviousbutton.hidden = YES;
        self.popupclosebutton.hidden = YES;
        self.loadingview.hidden = NO;
        [self.loadingview startAnimating];
    }
    else
    {
        
        float width = self.popupimage.image.size.width * (float)295 / self.popupimage.image.size.height;
        if(width < [[UIScreen mainScreen] bounds].size.width)
        {
            self.popupimage.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - width) / 2, ([[UIScreen mainScreen] bounds].size.height - 30 - 295) / 2, width, 295);
            
            self.popuplabel.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.size.height + self.popupimage.frame.origin.y, width, 30);
            
            self.popupclosebutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 33, self.popupimage.frame.origin.y, 33, 33);
            
            self.popupnextbutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 33, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 33) / 2), 33, 33);
            
            self.popuppreviousbutton.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 33) / 2), 33, 33);
        }
        else
        {
            float height = self.popupimage.image.size.height * (float)320 / self.popupimage.image.size.width;
            
            self.popupimage.frame = CGRectMake(0, self.popupimage.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, height);
            
            self.popuplabel.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.size.height + self.popupimage.frame.origin.y, self.popupimage.frame.size.width, 30);
            
            self.popupclosebutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 33, self.popupimage.frame.origin.y, 33, 33);
            
            self.popupnextbutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 33, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 33) / 2), 33, 33);
            
            self.popuppreviousbutton.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 33) / 2), 33, 33);
        }
    }
    
    if(istechnical)
    {
        if(self.technicalImageArr.count <= 1 && self.popupimage.image != nil)
        {
            self.popupnextbutton.hidden = YES;
            self.popuppreviousbutton.hidden = YES;
            self.popupclosebutton.hidden = NO;
            self.loadingview.hidden = YES;
        }
        else
        {
            if(inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = YES;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.technicalImageArr.count -1 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = YES;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.technicalImageArr.count -1 && inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
        }
        
    }
    else
    {
        if(self.galleryImageArr.count <= 1 && self.popupimage.image != nil)
        {
            self.popupnextbutton.hidden = YES;
            self.popuppreviousbutton.hidden = YES;
            self.popupclosebutton.hidden = NO;
            self.loadingview.hidden = YES;
        }
        else
        {
            if(inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = YES;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.galleryImageArr.count -1 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = YES;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.galleryImageArr.count -1 && inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
        }
    }
}

-(IBAction)closepopup:(id)sender
{
    [self.popupview removeFromSuperview];
    self.popupnextbutton.hidden = YES;
    self.popuppreviousbutton.hidden = YES;
    self.popupclosebutton.hidden = YES;
    self.popupimage.imageURL = nil;
    [checkimagetimer invalidate];
}

-(IBAction)next:(id)sender
{
    self.popupimage.imageURL = nil;
    if(istechnical)
    {
        inviewpage++;
        
        NSDictionary* technicalImage = [technicalImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
    else
    {
        inviewpage++;
        
        NSDictionary* technicalImage = [galleryImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
}

-(IBAction)previous:(id)sender
{
    self.popupimage.imageURL = nil;
    if(istechnical)
    {
        inviewpage--;
        NSDictionary* technicalImage = [technicalImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
    else
    {
        inviewpage--;
        NSDictionary* technicalImage = [galleryImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
}

//addnewjob
-(void)addjobviewanimationin
{
    self.jobtable.frame = CGRectMake(30, 95, 290, 278);
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(-[[UIScreen mainScreen] bounds].size.width, 0);
        self.jobview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             jobtransform = self.jobview.transform;
                             numberlayer = 1;
                             joblist = [db retrieveMyjob];
                             [self.jobtable reloadData];
                         }
                     }];
    [UIView commitAnimations];
}

-(void)addjobviewanimationout
{
    self.jobview.transform = jobtransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation([[UIScreen mainScreen] bounds].size.width, 0);
        self.jobview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             jobtransform = self.jobview.transform;
                             [self detailviewanimationin];
                         }
                     }];
    [UIView commitAnimations];
}

-(IBAction)newjob:(id)sender
{
    alert = [[UIAlertView alloc] initWithTitle:@"\n" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    newjobtitletextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 15.0, 260.0, 25.0)] autorelease];
    newjobtitletextfield.delegate = self;
    [newjobtitletextfield becomeFirstResponder];
    [newjobtitletextfield setBackgroundColor:[UIColor whiteColor]];
    newjobtitletextfield.textAlignment=UITextAlignmentCenter;
    newjobtitletextfield.layer.cornerRadius=5.0;
    [alert addSubview:newjobtitletextfield];
    [alert show];
}

-(IBAction)donejob:(id)sender
{
    [self addjobviewanimationout];
}

-(IBAction)addproductinjob:(id)sender
{
    buttontag = [db retrieveMyjobselectedrow:[sender tag]];
    if([db countProductinjob:buttontag :[self.codelabel.text substringFromIndex:7]]> 0)
    {
        int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
        [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :quantity+1];
        /*isadd = YES;
        isexist = YES;
        alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
        quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
        quantitytextfield.delegate = self;
        quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
        [quantitytextfield becomeFirstResponder];
        [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
        quantitytextfield.text = @"1";
        quantitytextfield.textAlignment=UITextAlignmentCenter;
        quantitytextfield.layer.cornerRadius=5.0;
        [alertaddjob addSubview:quantitytextfield];
        [alertaddjob show];
        [alertaddjob release];*/
    }
    else
    {
        NSError* error;
        
        NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
        
        NSDictionary* singleItem = [itemInfo objectAtIndex:0];
        NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
        
        NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
        
        Product *product = [[Product alloc] init];
        product.producttitle = self.titlelabel.text;
        product.productcode = [self.codelabel.text substringFromIndex:7];;
        product.producttrade = [self.tradelabel.text substringFromIndex:8];
        product.productretail = [self.retaillabel.text substringFromIndex:9];
        product.productdescription = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"'" withString:@""];
        product.productquantity = 1;
        product.productimagepathS = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]];
        product.productimagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
        product.myjobid = buttontag;
        product.sectionid = [[singleDetails objectForKey:@"application_id"] intValue];
        product.categoryid = [[singleDetails objectForKey:@"id"] intValue];
        product.myjobid = buttontag;
        [db insertProduct:product];
        [product release];
        
        /*isadd = YES;
        isexist = NO;
        alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
        quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
        quantitytextfield.delegate = self;
        quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
        [quantitytextfield becomeFirstResponder];
        [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
        quantitytextfield.text = @"1";
        quantitytextfield.textAlignment=UITextAlignmentCenter;
        quantitytextfield.layer.cornerRadius=5.0;
        [alertaddjob addSubview:quantitytextfield];
        [alertaddjob show];
        [alertaddjob release];*/
    }
    joblist = [db retrieveMyjob];
    [self.jobtable reloadData];
}

-(IBAction)removeproductinjob:(id)sender
{
    isadd = NO;
    buttontag = [db retrieveMyjobselectedrow:[sender tag]];
    int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
    [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :quantity-1];
    joblist = [db retrieveMyjob];
    [self.jobtable reloadData];
    /*alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be reduce" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reduce", nil];
    quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
    quantitytextfield.delegate = self;
    quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
    [quantitytextfield becomeFirstResponder];
    [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
    quantitytextfield.text = [NSString stringWithFormat:@"%d", quantity];
    quantitytextfield.textAlignment=UITextAlignmentCenter;
    quantitytextfield.layer.cornerRadius=5.0;
    [alertaddjob addSubview:quantitytextfield];
    [alertaddjob show];
    [alertaddjob release];*/
}

-(IBAction)quantityadd:(id)sender
{
    buttontag = [db retrieveMyjobselectedrow:[sender tag]];
    if([db countProductinjob:buttontag :[self.codelabel.text substringFromIndex:7]]> 0)
    {
        isadd = YES;
        isexist = YES;
        alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
        quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
        quantitytextfield.delegate = self;
        quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
        [quantitytextfield becomeFirstResponder];
        [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
        quantitytextfield.text = @"1";
        quantitytextfield.textAlignment=UITextAlignmentCenter;
        quantitytextfield.layer.cornerRadius=5.0;
        [alertaddjob addSubview:quantitytextfield];
        [alertaddjob show];
        [alertaddjob release];
    }
    else
    {
        isadd = YES;
        isexist = NO;
        alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
        quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
        quantitytextfield.delegate = self;
        quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
        [quantitytextfield becomeFirstResponder];
        [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
        quantitytextfield.text = @"1";
        quantitytextfield.textAlignment=UITextAlignmentCenter;
        quantitytextfield.layer.cornerRadius=5.0;
        [alertaddjob addSubview:quantitytextfield];
        [alertaddjob show];
        [alertaddjob release];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView == alertenquiry)
    {
        [self enquiryviewanimationout];
    }
    else if(alertView ==alertaddjob)
    {
        if(buttonIndex == 0)
        {
            [alertaddjob dismissWithClickedButtonIndex:0 animated:NO];
        }
        else
        {
            BOOL valid;
            NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
            NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:quantitytextfield.text];
            valid = [alphaNums isSupersetOfSet:inStringSet];
            if (valid)
            {
                if(isadd)
                {
                    if(isexist)
                    {
                        int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
                        [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :[quantitytextfield.text intValue] + quantity];
                    }
                    else
                    {
                        NSError* error;
                        
                        NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
                        
                        NSDictionary* singleItem = [itemInfo objectAtIndex:0];
                        NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
                        
                        NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
                        
                        NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
                        
                        Product *product = [[Product alloc] init];
                        product.producttitle = self.titlelabel.text;
                        product.productcode = [self.codelabel.text substringFromIndex:7];;
                        product.producttrade = [self.tradelabel.text substringFromIndex:8];
                        product.productretail = [self.retaillabel.text substringFromIndex:9];
                        product.productdescription = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"'" withString:@""];
                        product.productquantity = [quantitytextfield.text intValue];
                        product.productimagepathS = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]];
                        product.productimagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
                        product.myjobid = buttontag;
                        product.sectionid = [[singleDetails objectForKey:@"application_id"] intValue];
                        product.categoryid = [[singleDetails objectForKey:@"id"] intValue];
                        product.myjobid = buttontag;
                        [db insertProduct:product];
                        [product release];
                    }
                    joblist = [db retrieveMyjob];
                    [self.jobtable reloadData];
                    alert = [[UIAlertView alloc] initWithTitle:nil message:@"Product quantity has been increased！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
                    if([quantitytextfield.text intValue] > quantity)
                    {
                        [self.view makeToast:@"Cannot remove more than in database!" duration:(0.3) position:@"center"];
                        [quantitytextfield becomeFirstResponder];
                        [alertaddjob show];
                    }
                    else
                    {
                        [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :quantity -[quantitytextfield.text intValue]];
                        joblist = [db retrieveMyjob];
                        [self.jobtable reloadData];
                        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Product quantity has been reduced！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }
            else
            {
                [self.view makeToast:@"Insert only numeric!" duration:(0.3) position:@"center"];
                [quantitytextfield becomeFirstResponder];
                [alertaddjob show];
            }
        }
    }
    else
    {
        if(buttonIndex == 0)
        {
            [alert dismissWithClickedButtonIndex:0 animated:NO];
        }
        else
        {
            NSString *check = [newjobtitletextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(![check isEqualToString:@""])
            {
                if([check length] > 32)
                {
                    [self.view makeToast:@"Job name is too, please shorten it!" duration:(0.3) position:@"center"];
                    [newjobtitletextfield becomeFirstResponder];
                    [alert show];
                }
                else
                {
                    [alert dismissWithClickedButtonIndex:1 animated:NO];
                    [self.view makeToast:@"Successfully created new job！" duration:(0.3) position:@"center"];
                    NSDate *today = [NSDate date];
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    dateFormat.locale = [NSLocale currentLocale];
                    [dateFormat setDateFormat:@"EEEE, dd MMMM YYYY"];
                    NSString *dateString = [dateFormat stringFromDate:today];
                    [dateFormat release];
                    
                    Myjob *myjob = [[Myjob alloc] init];
                    myjob.myjobtitle = [newjobtitletextfield.text stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
                    myjob.myjobdate = dateString;
                    [db insertMyjob:myjob];
                    [myjob release];
                    
                    joblist = [db retrieveMyjob];
                    [self.jobtable reloadData];
                }
            }
            else
            {
                [self.view makeToast:@"Please give new job a name!" duration:(0.3) position:@"center"];
                [newjobtitletextfield becomeFirstResponder];
                [alert show];
            }
        }
    }
}

//enquiry view//
-(void)enquiryviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(-[[UIScreen mainScreen] bounds].size.width, 0);
        self.enquiryview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             enquirytransform = self.enquiryview.transform;
                             numberlayer = 1;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)enquiryviewanimationout
{
    self.enquiryview.transform = enquirytransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation([[UIScreen mainScreen] bounds].size.width, 0);
        self.enquiryview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             enquirytransform = self.enquiryview.transform;
                             [self detailviewanimationin];
                         }
                     }];
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [popfirstnametextfield resignFirstResponder];
    [poplastnametextfield resignFirstResponder];
    [popemailtextfield resignFirstResponder];
    [popaddresstextfield resignFirstResponder];
    [popcompanytextfield resignFirstResponder];
    [popphonetextfield resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.frame.size.height + textField.frame.origin.y > self.view.frame.size.height - 374)
    {
        float covered = textField.frame.size.height + textField.frame.origin.y - (self.view.frame.size.height - 414);
        CGPoint bottomOffset = CGPointMake(0, covered);
        [self.popscrollview setContentOffset: bottomOffset animated: YES];
    }
    self.popscrollview.contentSize = CGSizeMake(self.popscrollview.contentSize.width, self.popscrollview.contentSize.height + 200);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.popscrollview.contentSize = CGSizeMake(290, 570);
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]){
        [popcommentstextview resignFirstResponder];
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView.frame.size.height + textView.frame.origin.y > self.view.frame.size.height - 324)
    {
        float covered = textView.frame.size.height + textView.frame.origin.y - (self.view.frame.size.height - 324);
        CGPoint bottomOffset = CGPointMake(0, covered);
        [self.popscrollview setContentOffset: bottomOffset animated: YES];
    }
    self.popscrollview.contentSize = CGSizeMake(self.popscrollview.contentSize.width, self.popscrollview.contentSize.height + 200);
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.popscrollview.contentSize = CGSizeMake(290, 570);
}

-(IBAction)subcribe:(id)sender
{
    issubcribe = !issubcribe;
    if(issubcribe)
    {
        self.popselectedbutton.enabled = NO;
        self.popnotselectedbutton.enabled = YES;
        [self.popselectedbutton setBackgroundImage:[UIImage imageNamed:@"IsSelected.png"] forState:UIControlStateNormal];
        [self.popnotselectedbutton setBackgroundImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
    }
    else
    {
        self.popselectedbutton.enabled = YES;
        self.popnotselectedbutton.enabled = NO;
        [self.popselectedbutton setBackgroundImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
        [self.popnotselectedbutton setBackgroundImage:[UIImage imageNamed:@"IsSelected.png"] forState:UIControlStateNormal];
    }
}

-(IBAction)submit:(id)sender
{
    NSString *check = [popfirstnametextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![check isEqualToString:@""])
    {
        NSString *check = [poplastnametextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![check isEqualToString:@""])
        {
            NSString *check = [popemailtextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(![check isEqualToString:@""])
            {
                [GlobalFunction AddLoadingScreen:self];
                [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
            }
            else
                [self.view makeToast:@"Email is require！" duration:(0.3) position:@"center"];
        }
        else
            [self.view makeToast:@"Last Name is require！" duration:(0.3) position:@"center"];
    }
    else
        [self.view makeToast:@"First Name is require！" duration:(0.3) position:@"center"];
}

-(IBAction)cancel:(id)sender
{
    [self enquiryviewanimationout];
    [self performSelector:@selector(detailviewanimationin) withObject:nil afterDelay:0.4];
}

-(void)delay2
{
    NSString *decision;
    if(issubcribe)
        decision = @"Yes";
    else
        decision = @"No";
    WebServices *ws = [[WebServices alloc] init];
    
    NSString *reply = [ws sendEnquiry:popfirstnametextfield.text :poplastnametextfield.text :popemailtextfield.text :popphonetextfield.text :popcompanytextfield.text :popaddresstextfield.text :popproductcodetextfield.text :decision :popcommentstextview.text];
    
    [ws release];
    
    popfirstnametextfield.text = @"";
    poplastnametextfield.text = @"";
    popemailtextfield.text = @"";
    popphonetextfield.text = @"";
    popcommentstextview.text = @"";
    popcompanytextfield.text = @"";
    popaddresstextfield.text = @"";
    popproductcodetextfield.text = @"";
    issubcribe = YES;
    self.popselectedbutton.enabled = NO;
    self.popnotselectedbutton.enabled = YES;
    [self.popselectedbutton setBackgroundImage:[UIImage imageNamed:@"IsSelected.png"] forState:UIControlStateNormal];
    [self.popnotselectedbutton setBackgroundImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
    
    [GlobalFunction RemoveLoadingScreen:self];
    
    if([reply isEqualToString:@"Success"])
    {
        alertenquiry = [[UIAlertView alloc] initWithTitle:@"THANK YOU FOR YOUR INQUIRY" message:@"A CRH team member will contact you as soon as possible." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
    }
    else
    {
        alertenquiry = [[UIAlertView alloc] initWithTitle:@"ERROR OCCURED" message:@"Enquiry sent failed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    
    [alertenquiry show];
    [alertenquiry release];
}

-(IBAction)cancelemail:(id)sender
{
    if([sender tag] == 988)
    {
        [self priceviewanimationout];
        [self performSelector:@selector(detailviewanimationin) withObject:nil afterDelay:0.4];
    }
    else
    {
        [self typeviewanimationout];
        [self performSelector:@selector(priceviewanimationin) withObject:nil afterDelay:0.4];
    }
}

//email//
-(IBAction)sendwithprice:(id)sender
{
    isneedprice = YES;
    [self priceviewanimationout];
    [self performSelector:@selector(typeviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)sendwithoutprice:(id)sender
{
    isneedprice = NO;
    [self priceviewanimationout];
    [self performSelector:@selector(typeviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)sendplain:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:@"CRH Product Bookmarks"];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            NSString *emailBody;
            
            if(isneedprice)
                emailBody = [NSString stringWithFormat:@"\n\n\n%@\n\n%@\n\n%@\n\n%@\n\n%@", titleOfRowSelected, self.codelabel.text, self.tradelabel.text, self.retaillabel.text, descriptionlabel.text];
            else
                emailBody = [NSString stringWithFormat:@"\n\n\n%@\n\n%@\n\n%@", titleOfRowSelected, self.codelabel.text, descriptionlabel.text];
            [mailer setMessageBody:emailBody isHTML:NO];
            
            // only for iPad
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)sendhtml:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:@"CRH Product Bookmarks"];
            
            NSString *emailBody;
            NSString *filterNewLine = [NSString stringWithFormat:@"%@", [descriptionlabel.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
            
            if(isneedprice)
                emailBody = [NSString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@", imagepathM, titleOfRowSelected, self.codelabel.text, self.tradelabel.text, self.retaillabel.text, filterNewLine];
            else
                emailBody = [NSString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%@<br /><br />%@<br /><br />%@", imagepathM, titleOfRowSelected, self.codelabel.text, filterNewLine];
            
            [mailer setMessageBody:emailBody isHTML:YES];
            
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)sendpdf:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        [GlobalFunction AddLoadingScreen:self];
        [self.view makeToast:@"Generating PDF..." duration:(0.3) position:@"center"];
        [self performSelector:@selector(delay3) withObject:nil afterDelay:0.3];
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(void)delay3
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSString * timeStampValue = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
            
            NSString *try = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
            NSString *filterStr = [try stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
            
            NSString *filterDescStr = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
            
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"µm" withString:@"micrometer"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"©" withString:@"(copyrighted)"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
            
            NSString *filterTitleStr = [titleOfRowSelected stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
            
            WebServices *ws = [[WebServices alloc] init];
            
            NSString *reply = [[NSString alloc] init];
            
            if(isneedprice)
            {
                reply = [ws generatePDF_FileWithPrice:filterTitleStr :self.codelabel.text :self.tradelabel.text :self.retaillabel.text : filterDescStr :filterStr :timeStampValue];
            }
            else
            {
                reply = [ws generatePDF_FileWithoutPrice:filterTitleStr :self.codelabel.text:filterDescStr :filterStr :timeStampValue];
            }
            
            [ws release];
            
            if([reply isEqualToString:@"Success"])
            {
                NSString *pdf_link = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/tiseno/fpdf17/product_pdf/%@.pdf", timeStampValue];
                
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                [mailer setSubject:@"CRH Product Bookmarks"];
                
                NSString *emailBody = [NSString stringWithFormat:@"<br /><br /><br /><br />Click the link to download the pdf file :<br /> <a href=\"%@\">%@.pdf</a>", pdf_link, timeStampValue];
                
                [mailer setMessageBody:emailBody isHTML:YES];
                
                mailer.modalPresentationStyle = UIModalPresentationPageSheet;
                
                [self presentModalViewController:mailer animated:YES];
                
                [mailer release];
                
                [GlobalFunction RemoveLoadingScreen:self];
            }
            else
            {
                [self.view makeToast:@"Failed to generate PDF..." duration:(0.3) position:@"center"];
            }
            
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)priceviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -self.priceview.frame.size.height);
        self.priceview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             pricetransform = self.priceview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)priceviewanimationout
{
    self.priceview.transform = pricetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, self.priceview.frame.size.height);
        self.priceview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             pricetransform = self.priceview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)typeviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -self.typeview.frame.size.height);
        self.typeview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             typetransform = self.typeview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)typeviewanimationout
{
    self.typeview.transform = typetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, self.typeview.frame.size.height);
        self.typeview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             typetransform = self.typeview.transform;
                         }
                     }];
    [UIView commitAnimations];
}
///////////////

@end
