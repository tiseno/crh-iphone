//
//  ThumbnailScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "ThumbnailScene.h"

@implementation ThumbnailScene

@synthesize categoryheaderbg, thumbnailtable, categorylabel;

@synthesize categoryArr, categorytext, itemArr, totalItem, itemPriceArr, middleImageArr, tradeArr, row, rowselected, categoryrowselected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:1.0];
    
    self.categorylabel.text = self.categorytext;
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    red = [[appDelegate.colorcodered objectAtIndex:row] floatValue];
    green = [[appDelegate.colorcodegreen objectAtIndex:row] floatValue];
    blue = [[appDelegate.colorcodeblue objectAtIndex:row] floatValue];
    
    self.categoryheaderbg.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    self.categoryheaderbg.frame = CGRectMake(0, 0, 320, 44);
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.categoryheaderbg.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1] CGColor], [appDelegate.gradientbottom objectAtIndex:row], nil];
    [self.categoryheaderbg.layer insertSublayer:gradient atIndex:0];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.thumbnailtable reloadData];
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [categoryheaderbg release];
    [thumbnailtable release];
    [categorylabel release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *hlCellID = @"hlCellID";
    
    UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
    if(hlcell == nil) {
        hlcell =  [[[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID] autorelease];
        hlcell.accessoryType = UITableViewCellAccessoryNone;
        hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    UIView *existingToast = [hlcell.contentView viewWithTag:888];
    if (existingToast != nil) {
        [existingToast removeFromSuperview];
    }
    
    UIScrollView *scrollview = [[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, self.thumbnailtable.frame.size.height + 1)] autorelease];
    scrollview.backgroundColor = [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:1];
    scrollview.tag = 888;
    [hlcell.contentView addSubview:scrollview];
    int n = [self.itemArr count];
    int i=0, rows = 4, columns = 5;
    i1=0;
    
    int gapx = ([[UIScreen mainScreen] bounds].size.width - rows * 70) / (rows + 1);
    int gapy = (self.thumbnailtable.frame.size.height - columns * 75) / (columns + 1);
    
    while(i<n)
    {
        int yy = gapy + i1 * (75 + gapy);
        int j=0;
        for(j=0; j<columns;j++){
            if (i>=n) break;
            
            NSError* error;
            
            NSArray* itemInfo = [totalItem objectAtIndex:i];
            
            NSDictionary* singleItem = [itemInfo objectAtIndex:0];
            NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
            
            NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            UIView *thumbnailbackgroundview = [[[UIView alloc]initWithFrame:CGRectMake(gapx + j * (gapx + 70), yy, 70, 75)] autorelease];
            thumbnailbackgroundview.tag = i;
            thumbnailbackgroundview.backgroundColor = [UIColor whiteColor];
            UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galleryview:)];
            [thumbnailbackgroundview addGestureRecognizer:singletap];
            [thumbnailbackgroundview setUserInteractionEnabled:YES];
            
            UIImageView *thumbnailview = [[UIImageView alloc]initWithFrame:CGRectMake(11, 0, 49, 62)];
            thumbnailview.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [thumbnailbackgroundview addSubview:thumbnailview];
            [thumbnailview release];
            
            UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(0, 62, 70, 13)];
            Title.textAlignment = UITextAlignmentCenter;
            Title.text = [itemCode objectForKey:@"value"];
            Title.textColor = [UIColor whiteColor];
            Title.backgroundColor = [UIColor blackColor];
            Title.font = [UIFont systemFontOfSize:9];
            Title.lineBreakMode = UILineBreakModeWordWrap;
            Title.numberOfLines = 0;
            [thumbnailbackgroundview addSubview:Title];
            [Title release];
            
            [scrollview addSubview:thumbnailbackgroundview];
            i++;
        }
        i1 = i1+1;
    }
    
    if(gapy + i1 * (175 + gapy) > self.thumbnailtable.frame.size.height)
        scrollview.contentSize = CGSizeMake(320, gapy + i1 * (75 + gapy));
    else
        scrollview.contentSize = CGSizeMake(320, self.thumbnailtable.frame.size.height);
    return hlcell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return self.thumbnailtable.frame.size.height;
}

-(void)galleryview:(UIGestureRecognizer *)gesture
{
    [GlobalFunction AddLoadingScreen:self];
    productrowselected = [gesture.view tag];
    [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(void)delay
{
    NSError* error;
    
    NSDictionary* novainfo = [self.categoryArr objectAtIndex:self.categoryrowselected];
    
    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    self.itemArr = [[ws selectAllFromCategoryItemTableByID:[[novadetails objectForKey:@"id"] intValue]] retain];
    
    self.totalItem = [[[NSMutableArray alloc] initWithCapacity:itemArr.count+1] retain];
    
    for(int i = 0; i <self.itemArr.count; i++)
    {
        NSDictionary* itemInfo = [self.itemArr objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"category_item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [self.totalItem addObject:item];
    }
    
    //create an array for price & an array for middle image @@
    self.itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    self.middleImageArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    self.tradeArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    
    for(int j = 0; j < self.totalItem.count; j++)
    {
        NSArray* itemInfo = [self.totalItem objectAtIndex:j];
        
        NSDictionary* test = [itemInfo objectAtIndex:0];
        
        NSDictionary* infoDetails = [test objectForKey:@"item"];
        
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *test11 = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        NSDictionary *tradeKey = [elements objectForKey:@"953ff3d0-753c-47f7-aeec-96599ecc4814"];
        NSDictionary *trade = [tradeKey objectForKey:@"0"];
        
        [self.middleImageArr addObject:[test11 objectForKey:@"file"]];
        
        [self.itemPriceArr addObject:[price objectForKey:@"value"]];
        
        [self.tradeArr addObject:[trade objectForKey:@"value"]];
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];

    ProductScene *GtestClasssViewController=[[[ProductScene alloc] initWithNibName:@"ProductScene"  bundle:nil] autorelease];
    GtestClasssViewController.categorytext = self.categorylabel.text;
    GtestClasssViewController.row = row;
    GtestClasssViewController.rowselected = productrowselected;
    GtestClasssViewController.isjump = YES;
    GtestClasssViewController.itemArr = self.itemArr;
    GtestClasssViewController.totalItem = self.totalItem;
    GtestClasssViewController.middleImageArr = self.middleImageArr;
    GtestClasssViewController.itemPriceArr = self.itemPriceArr;
    GtestClasssViewController.tradeArr = self.tradeArr;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

@end
