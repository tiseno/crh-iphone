//
//  CategoryScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "AppDelegate.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "CategoryCell.h"
#import "ProductScene.h"
#import "ThumbnailScene.h"

@interface CategoryScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UISearchBarDelegate>
{
    CategoryCell *categorycell;
    BOOL isconnected;
    float red, green, blue;
    int rowselected;
    UIAlertView *alert;
    NSTimer *connectiontimer;
    NSMutableArray *categorylist, *beforesearch;
    UIView *transparentview;
}

//main//
@property (retain, nonatomic) IBOutlet UIImageView *categoryheaderbg;
@property (retain, nonatomic) IBOutlet UITableView *categorytable;
@property (retain, nonatomic) IBOutlet UILabel *categorylabel;

@property (retain, nonatomic) IBOutlet UIView *searchview;
@property (retain, nonatomic) IBOutlet UISearchBar *searchbar;

-(IBAction)search:(id)sender;
-(IBAction)cancelsearch:(id)sender;
-(IBAction)back:(id)sender;

@property (retain, nonatomic) NSArray *categoryArr;
@property (retain, nonatomic) NSString *categorytext;
@property (retain, nonatomic) NSArray *itemArr;

@property (retain, nonatomic) NSMutableArray *totalItem;
@property (retain, nonatomic) NSMutableArray *itemPriceArr;
@property (retain, nonatomic) NSMutableArray *middleImageArr;
@property (retain, nonatomic) NSMutableArray *tradeArr;
@property (nonatomic) int row;

@end
