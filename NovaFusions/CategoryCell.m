//
//  CategoryCell.m
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

@synthesize title, separator, thumbnailbutton, gradient;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        separator = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 300, 37)];
        gradient = [CAGradientLayer layer];
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1] CGColor], (id)[[UIColor colorWithRed:33.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1] CGColor], nil];
        [separator.layer insertSublayer:gradient atIndex:0];
        [self addSubview:separator];
        
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(15, 2, 230, 35)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor whiteColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:12];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.title = Title;
        [Title release];
        [self addSubview:title];
        
        thumbnailbutton = [[UIButton alloc] init];
        [self addSubview:thumbnailbutton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [title release];
    [separator release];
    [thumbnailbutton release];
    [super dealloc];
}

@end
