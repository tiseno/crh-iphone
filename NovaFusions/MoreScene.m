//
//  MoreScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define HelpTableHeight 60
#define HelpTableWidth 320

#import "MoreScene.h"

@implementation MoreScene

@synthesize topbgimageview, arrowimageview, aboutview, contactusview, helpview, helptable, closebutton;

@synthesize contactusimageview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        self.topbgimageview.frame = CGRectMake(0, 0, 320, 44);
        self.aboutview.frame = CGRectMake(0, 44, 320, [[UIScreen mainScreen] bounds].size.height - 108);
        self.contactusview.frame = CGRectMake(0, 44, 320, [[UIScreen mainScreen] bounds].size.height - 108);
        self.helpview.frame = CGRectMake(0, 44, 320, [[UIScreen mainScreen] bounds].size.height - 108);
        self.closebutton.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 64, 320, 44);
        self.contactusimageview.frame = CGRectMake(0, 0, 320, 262);
    }
    
    
    self.view.backgroundColor = [UIColor colorWithRed:126.0 / 255.0 green:7.0 / 255.0 blue:1.0 / 255.0 alpha:1.0];
    self.aboutview.backgroundColor = [UIColor clearColor];
    self.contactusview.backgroundColor = [UIColor clearColor];
    self.helpview.backgroundColor = [UIColor clearColor];
    
    self.topbgimageview.backgroundColor = [UIColor colorWithRed:28.0 / 255.0 green:28.0 / 255.0 blue:28.0 / 255.0 alpha:1.0];
    self.helptable.backgroundColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [topbgimageview release];
    [arrowimageview release];
    [aboutview release];
    [contactusview release];
    [helpview release];
    [helptable release];
    [closebutton release];

    [contactusimageview release];
    
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 14;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    helpcell = (HelpCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(helpcell == nil){
        helpcell = [[[HelpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    helpcell.helpdescription.text = [appDelegate.helpdescription objectAtIndex:indexPath.row];
    helpcell.imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_help_%d.png", indexPath.row + 1]];
    
    if(indexPath.row > 7)
    {
        helpcell.imageview.frame = CGRectMake(20, 20, 100, 37);
        helpcell.helpdescription.frame = CGRectMake(125, 10, 175, 55);
        helpcell.dotimage.frame = CGRectMake(0, 73, 320, 2);
        helpcell.frame = CGRectMake(0, 0, 320, 75);
    }
    else
    {
        helpcell.imageview.frame = CGRectMake(20, 20, 42, 28);
        helpcell.helpdescription.frame = CGRectMake(70, 10, 230, 40);
        helpcell.dotimage.frame = CGRectMake(0, 58, 320, 2);
        helpcell.frame = CGRectMake(0, 0, HelpTableWidth, HelpTableHeight);
    }
    return helpcell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    helpcell = (HelpCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return helpcell.frame.size.height;
}

-(IBAction)about:(id)sender
{
    self.arrowimageview.frame = CGRectMake(41, 32, 25, 12);
    self.aboutview.hidden = NO;
    self.contactusview.hidden = YES;
    self.helpview.hidden = YES;
}

-(IBAction)contactus:(id)sender
{
    self.arrowimageview.frame = CGRectMake(148, 32, 25, 12);
    self.contactusview.hidden = NO;
    self.aboutview.hidden = YES;
    self.helpview.hidden = YES;
}

-(IBAction)help:(id)sender
{
    self.arrowimageview.frame = CGRectMake(255, 32, 25, 12);
    self.helpview.hidden = NO;
    self.aboutview.hidden = YES;
    self.contactusview.hidden = YES;
}

-(IBAction)close:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

//contactus//
-(IBAction)twittercontactus:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"https://twitter.com/crhaus" ];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)facebookcontactus:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.facebook.com/crh.australia" ];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)linkedcontactus:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://au.linkedin.com/pub/crh-australia/5a/b14/2a1" ];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
