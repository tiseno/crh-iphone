//
//  SectionCell.m
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "SectionCell.h"

@implementation SectionCell
@synthesize title, description, bgview;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        bgview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 68)];
        [self addSubview:bgview];
        
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(50, 3, 225, 32)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor blackColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:13];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.title = Title;
        [Title release];
        [self addSubview:title];
        
        UILabel *Description = [[UILabel alloc] initWithFrame:CGRectMake(50, 35, 240, 30)];
        Description.textAlignment = UITextAlignmentLeft;
        Description.textColor = [UIColor grayColor];
        Description.backgroundColor = [UIColor clearColor];
        Description.font = [UIFont systemFontOfSize:11];
        Description.numberOfLines = 2;
        self.description = Description;
        [Description release];
        [self addSubview:description];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

-(void)dealloc
{
    [title release];
    [description release];
    [bgview release];
    [super dealloc];
}

@end
