//
//  AppDelegate.h
//  NovaFusions
//
//  Created by tiseno on 10/22/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSNumber *red, *green, *blue;
    NSString *descriptiontext;
    id bottom;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, retain) NSMutableArray *colorcodered, *colorcodegreen, *colorcodeblue, *gradientbottom, *helpdescription;

@end
