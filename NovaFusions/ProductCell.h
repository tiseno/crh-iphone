//
//  ProductCell.h
//  NovaFusions
//
//  Created by tiseno on 10/24/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *title, *code, *description, *serial;
@property (nonatomic, retain) UIImageView *productimage, *serialimage;
@property (nonatomic, retain) UIView *backgroundview;

@end
