//
//  ThumbnailScene.h
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AsyncImageView.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "ProductScene.h"

@interface ThumbnailScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    BOOL isconnected;
    float red, green, blue;
    int i1, productrowselected;
    UIAlertView *alert;
    NSTimer *connectiontimer;
}

@property (retain, nonatomic) IBOutlet UIImageView *categoryheaderbg;
@property (retain, nonatomic) IBOutlet UITableView *thumbnailtable;
@property (retain, nonatomic) IBOutlet UILabel *categorylabel;

-(IBAction)back:(id)sender;

@property (retain, nonatomic) NSArray *categoryArr;
@property (retain, nonatomic) NSString *categorytext;
@property (retain, nonatomic) NSArray *itemArr;

@property (retain, nonatomic) NSMutableArray *totalItem;
@property (retain, nonatomic) NSMutableArray *itemPriceArr;
@property (retain, nonatomic) NSMutableArray *middleImageArr;
@property (retain, nonatomic) NSMutableArray *tradeArr;
@property (nonatomic) int row, rowselected, categoryrowselected;

@end
