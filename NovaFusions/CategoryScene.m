//
//  CategoryScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define CategoryTableHeight 41
#define MainTableWidth 320

#import "CategoryScene.h"

@implementation CategoryScene

@synthesize categoryheaderbg, categorytable, categorylabel, searchbar, searchview;

@synthesize categoryArr, categorytext, itemArr, totalItem, itemPriceArr, middleImageArr, tradeArr, row;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 2)] ;
    transparentview.backgroundColor = [UIColor clearColor];
    self.categorytable.tableHeaderView = transparentview;
    [self.categorytable setBackgroundColor:[UIColor blackColor]];
    self.categorytable.tableFooterView = [[[UIView alloc] init] autorelease];
    
    self.categorylabel.text = self.categorytext;
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    red = [[appDelegate.colorcodered objectAtIndex:row] floatValue];
    green = [[appDelegate.colorcodegreen objectAtIndex:row] floatValue];
    blue = [[appDelegate.colorcodeblue objectAtIndex:row] floatValue];
    
    self.categoryheaderbg.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    self.categoryheaderbg.frame = CGRectMake(0, 0, 320, 44);
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.categoryheaderbg.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1] CGColor], [appDelegate.gradientbottom objectAtIndex:row], nil];
    [self.categoryheaderbg.layer insertSublayer:gradient atIndex:0];
    
    [[self.searchbar.subviews objectAtIndex:0] removeFromSuperview];
    
    beforesearch = [self.categoryArr mutableCopy];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [categoryheaderbg release];
    [categorytable release];
    [categorylabel release];
    [searchbar release];
    [searchview release];
    [transparentview release];
    
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.categoryArr.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSDictionary* novainfo = [self.categoryArr objectAtIndex:indexPath.row];
    
    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    categorycell= (CategoryCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(categorycell == nil){
        categorycell = [[[CategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    
    categorycell.selectionStyle = UITableViewCellSelectionStyleNone;
    categorycell.title.text = [novadetails objectForKey:@"name"];
    
    CGSize maxsize = CGSizeMake(230, 9999);
    UIFont *thefont = [UIFont boldSystemFontOfSize:12];
    CGSize textsize = [categorycell.title.text sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    
    categorycell.frame = CGRectMake(0, 0, MainTableWidth, textsize.height + 22);
    categorycell.separator.frame = CGRectMake(0, 2, MainTableWidth, categorycell.frame.size.height-4);
    categorycell.title.frame = CGRectMake(20, 12, 230, textsize.height);
    
    categorycell.thumbnailbutton.frame = CGRectMake(284, (categorycell.frame.size.height - 22) / 2 + 2, 26, 22);
    categorycell.thumbnailbutton.tag = indexPath.row;
    [categorycell.thumbnailbutton setBackgroundImage:[UIImage imageNamed:@"icon_grid.png"] forState:UIControlStateNormal];
    [categorycell.thumbnailbutton addTarget:self action:@selector(thumbnailview:) forControlEvents:UIControlEventTouchUpInside];
    
    [categorycell.separator setBackgroundColor:[UIColor colorWithRed:red / 255.0 green:green/255.0 blue:blue/255.0 alpha:1]];
    categorycell.gradient.frame = CGRectMake(274, 0, 46, categorycell.frame.size.height-4);
    
    return categorycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [GlobalFunction AddLoadingScreen:self];
    categorycell= (CategoryCell*)[tableView cellForRowAtIndexPath:indexPath];
    rowselected = indexPath.row;
    [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    categorycell= (CategoryCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return categorycell.frame.size.height;
}

-(IBAction)thumbnailview:(id)sender
{
    [GlobalFunction AddLoadingScreen:self];
    rowselected = [sender tag];
    [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)search:(id)sender
{
    self.searchview.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    transparentview.frame = CGRectMake(0, 0, 320, 46);
    [transparentview addSubview:self.searchview];
    self.categorytable.tableHeaderView = transparentview;
    [self.categorytable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(IBAction)cancelsearch:(id)sender
{
    [self.searchview removeFromSuperview];
    transparentview.frame = CGRectMake(0, 0, 320, 2);
    self.categorytable.tableHeaderView = transparentview;
    [self resetSearch];
}

//search//
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchbar resignFirstResponder];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        [self resetSearch];
        [self.categorytable reloadData];
        return;
    }
    [self filterContentForSearchText:searchText];
}

-(void)resetSearch
{
    self.categoryArr = beforesearch;
    [self.categorytable reloadData];
}

- (void)filterContentForSearchText:(NSString*)searchText
{
	categorylist = [self.categoryArr mutableCopy];
	[categorylist removeAllObjects];
    
	for (int i = 0; i < self.categoryArr.count;i++)
	{
        NSDictionary* novainfo = [self.categoryArr objectAtIndex:i];
        
        NSDictionary* novadetails = [novainfo objectForKey:@"category"];
        
        NSString *categorytitle = [novadetails objectForKey:@"name"];
        
        if ([categorytitle rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            continue;
        }
        else
        {
            [categorylist addObject:[self.categoryArr objectAtIndex:i]];
        }
	}
    
    self.categoryArr = categorylist;
    [self.categorytable reloadData];
}

-(void)delay
{
    NSError* error;
    
    NSDictionary* novainfo = [self.categoryArr objectAtIndex:rowselected];
    
    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    itemArr = [[ws selectAllFromCategoryItemTableByID:[[novadetails objectForKey:@"id"] intValue]] retain];
    
    totalItem = [[[NSMutableArray alloc] initWithCapacity:itemArr.count+1] retain];
    
    for(int i = 0; i <itemArr.count; i++)
    {
        NSDictionary* itemInfo = [itemArr objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"category_item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [totalItem addObject:item];
    }
    
    //create an array for price & an array for middle image @@
    itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    middleImageArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    tradeArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    
    for(int j = 0; j < totalItem.count; j++)
    {
        NSArray* itemInfo = [totalItem objectAtIndex:j];
        
        NSDictionary* test = [itemInfo objectAtIndex:0];
        
        NSDictionary* infoDetails = [test objectForKey:@"item"];
        
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *test11 = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        NSDictionary *tradeKey = [elements objectForKey:@"953ff3d0-753c-47f7-aeec-96599ecc4814"];
        NSDictionary *trade = [tradeKey objectForKey:@"0"];
        
        [middleImageArr addObject:[test11 objectForKey:@"file"]];
        
        [itemPriceArr addObject:[price objectForKey:@"value"]];
        
        [tradeArr addObject:[trade objectForKey:@"value"]];
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    ProductScene *GtestClasssViewController=[[[ProductScene alloc] initWithNibName:@"ProductScene"  bundle:nil] autorelease];
    GtestClasssViewController.categorytext = self.categorylabel.text;
    GtestClasssViewController.row = row;
    GtestClasssViewController.itemArr = self.itemArr;
    GtestClasssViewController.totalItem = self.totalItem;
    GtestClasssViewController.middleImageArr = self.middleImageArr;
    GtestClasssViewController.itemPriceArr = self.itemPriceArr;
    GtestClasssViewController.tradeArr = self.tradeArr;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}
 
-(void)delay2
{
    NSError* error;
    
    NSDictionary* novainfo = [self.categoryArr objectAtIndex:rowselected];
    
    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    itemArr = [[ws selectAllFromCategoryItemTableByID:[[novadetails objectForKey:@"id"] intValue]] retain];
    
    totalItem = [[[NSMutableArray alloc] initWithCapacity:itemArr.count+1] retain];
    
    for(int i = 0; i <itemArr.count; i++)
    {
        NSDictionary* itemInfo = [itemArr objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"category_item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [totalItem addObject:item];
    }
    
    //create an array for price & an array for middle image @@
    itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    middleImageArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    tradeArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    
    for(int j = 0; j < totalItem.count; j++)
    {
        NSArray* itemInfo = [totalItem objectAtIndex:j];
        
        NSDictionary* test = [itemInfo objectAtIndex:0];
        
        NSDictionary* infoDetails = [test objectForKey:@"item"];
        
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *test11 = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        NSDictionary *tradeKey = [elements objectForKey:@"953ff3d0-753c-47f7-aeec-96599ecc4814"];
        NSDictionary *trade = [tradeKey objectForKey:@"0"];
        
        [middleImageArr addObject:[test11 objectForKey:@"file"]];
        
        [itemPriceArr addObject:[price objectForKey:@"value"]];
        
        [tradeArr addObject:[trade objectForKey:@"value"]];
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    ThumbnailScene *GtestClasssViewController=[[[ThumbnailScene alloc] initWithNibName:@"ThumbnailScene"  bundle:nil] autorelease];
    GtestClasssViewController.categoryArr = self.categoryArr;
    GtestClasssViewController.categorytext = self.categorylabel.text;
    GtestClasssViewController.row = row;
    GtestClasssViewController.categoryrowselected = rowselected;
    GtestClasssViewController.itemArr = self.itemArr;
    GtestClasssViewController.totalItem = self.totalItem;
    GtestClasssViewController.middleImageArr = self.middleImageArr;
    GtestClasssViewController.itemPriceArr = self.itemPriceArr;
    GtestClasssViewController.tradeArr = self.tradeArr;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

@end
