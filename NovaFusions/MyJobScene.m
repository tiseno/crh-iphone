//
//  MyJobScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define MyJobTableWidth 320

#import "MyJobScene.h"

@implementation MyJobScene

@synthesize jobtable, titlelabel, topbar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[UIScreen mainScreen] bounds].size.height > 480)
        self.topbar.frame = CGRectMake(0, 0, 320, 44);
    [self.topbar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"more_top_bar.png"]] autorelease] atIndex:1];
    self.jobtable.tableFooterView = [[[UIView alloc] init] autorelease];
    self.titlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    db = [[DatabaseAction alloc] init];
    
    joblist = [db retrieveMyjob];
    [self.jobtable reloadData];
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [jobtable release];
    [titlelabel release];
    [topbar release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return joblist.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    myjobcell = (MyJobCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(myjobcell == nil){
        myjobcell = [[[MyJobCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    
    myjobcell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    joblist = [db retrieveMyjob];
    Myjob *myjob = [joblist objectAtIndex:indexPath.row];
    myjobcell.myjobtitle.text = [myjob.myjobtitle stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
    myjobcell.myjobdate.text = myjob.myjobdate;
    
    CGSize maxsize = CGSizeMake(280, 9999);
    UIFont *thefont = [UIFont boldSystemFontOfSize:14];
    CGSize textsize = [myjobcell.myjobtitle.text sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    
    myjobcell.myjobtitle.frame = CGRectMake(20, 5, 200, textsize.height);
    myjobcell.myjobdate.frame = CGRectMake(20, myjobcell.myjobtitle.frame.origin.y + myjobcell.myjobtitle.frame.size.height + 5, 200, 15);
    myjobcell.frame = CGRectMake(0, 0, MyJobTableWidth, 40 + textsize.height);
    CGRect imageframe = CGRectMake(290, (myjobcell.frame.size.height - myjobcell.arrowimage.frame.size.height) / 2, myjobcell.arrowimage.frame.size.width, myjobcell.arrowimage.frame.size.height);
    myjobcell.arrowimage.frame = imageframe;
    
    return myjobcell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    myjobcell = (MyJobCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    InfoScene *GtestClasssViewController=[[[InfoScene alloc] initWithNibName:@"InfoScene"  bundle:nil] autorelease];
    GtestClasssViewController.jobrowselected = indexPath.row;
    GtestClasssViewController.jobtitle = myjobcell.myjobtitle.text;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    myjobcell = (MyJobCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return myjobcell.frame.size.height;
}

-(IBAction)addjob:(id)sender
{
    alert = [[UIAlertView alloc] initWithTitle:@"\n" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    newjobtitletextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 15.0, 260.0, 25.0)] autorelease];
    newjobtitletextfield.delegate = self;
    [newjobtitletextfield becomeFirstResponder];
    [newjobtitletextfield setBackgroundColor:[UIColor whiteColor]];
    newjobtitletextfield.textAlignment=UITextAlignmentCenter;
    newjobtitletextfield.layer.cornerRadius=5.0;
    [alert addSubview:newjobtitletextfield];
    [alert show];
    [alert release];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }else{
        NSString *check = [newjobtitletextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![check isEqualToString:@""])
        {
            if([check length] > 32)
            {
                [self.view makeToast:@"Job name is too, please shorten it!" duration:(0.3) position:@"center"];
                [newjobtitletextfield becomeFirstResponder];
                [alert show];
            }
            else
            {
                [alert dismissWithClickedButtonIndex:1 animated:NO];
                [self.view makeToast:@"Successfully created new job！" duration:(0.3) position:@"center"];
                NSDate *today = [NSDate date];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                dateFormat.locale = [NSLocale currentLocale];
                [dateFormat setDateFormat:@"EEEE, dd MMMM YYYY"];
                NSString *dateString = [dateFormat stringFromDate:today];
                [dateFormat release];
                
                Myjob *myjob = [[Myjob alloc] init];
                myjob.myjobtitle = [newjobtitletextfield.text stringByReplacingOccurrencesOfString:@"'" withString:@"\""];;
                myjob.myjobdate = dateString;
                [db insertMyjob:myjob];
                [myjob release];
                
                joblist = [db retrieveMyjob];
                [self.jobtable reloadData];
            }
        }
        else
        {
            [self.view makeToast:@"Please give new job a name!" duration:(0.3) position:@"center"];
            [newjobtitletextfield becomeFirstResponder];
            [alert show];
        }
    }
}

@end
