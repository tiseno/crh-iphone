//
//  SearchCell.m
//  NovaFusions
//
//  Created by tiseno on 10/31/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell

@synthesize titlelabel, codelabel, retaillabel, productimage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 220, 25)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor blackColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:13];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.titlelabel = Title;
        [Title release];
        [self addSubview:titlelabel];
        
        UILabel *Code = [[UILabel alloc] initWithFrame:CGRectMake(90, 40, 170, 15)];
        Code.textAlignment = UITextAlignmentLeft;
        Code.textColor = [UIColor grayColor];
        Code.backgroundColor = [UIColor clearColor];
        Code.font = [UIFont boldSystemFontOfSize:11];
        Code.lineBreakMode = UILineBreakModeWordWrap;
        Code.numberOfLines = 1;
        self.codelabel = Code;
        [Code release];
        [self addSubview:codelabel];
        
        UILabel *Retail = [[UILabel alloc] initWithFrame:CGRectMake(90, 55, 170, 15)];
        Retail.textAlignment = UITextAlignmentLeft;
        Retail.textColor = [UIColor grayColor];
        Retail.backgroundColor = [UIColor clearColor];
        Retail.font = [UIFont boldSystemFontOfSize:11];
        Retail.lineBreakMode = UILineBreakModeWordWrap;
        Retail.numberOfLines = 1;
        self.retaillabel = Retail;
        [Retail release];
        [self addSubview:retaillabel];
        
        UIImageView *ProductImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 83, 104)];
        self.productimage = ProductImage;
        [ProductImage release];
        [self addSubview:productimage];
        
        UIImageView *ArrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(280, 38, 30, 30)];
        ArrowImage.image = [UIImage imageNamed:@"search_arrow.png"];
        [self addSubview:ArrowImage];
        [ArrowImage release];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [titlelabel release];
    [codelabel release];
    [retaillabel release];
    [productimage release];
    [super dealloc];
}

@end
