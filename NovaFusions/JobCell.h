//
//  JobCell.h
//  NovaFusions
//
//  Created by tiseno on 10/25/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *jobtitle, *jobdescription;

@end
