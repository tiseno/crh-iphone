//
//  HelpCell.m
//  NovaFusions
//
//  Created by tiseno on 10/31/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "HelpCell.h"

@implementation HelpCell

@synthesize helpdescription, imageview, dotimage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *Description = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, 230, 40)];
        Description.textAlignment = UITextAlignmentLeft;
        Description.textColor = [UIColor whiteColor];
        Description.backgroundColor = [UIColor clearColor];
        Description.font = [UIFont systemFontOfSize:11];
        Description.lineBreakMode = UILineBreakModeWordWrap;
        Description.numberOfLines = 0;
        self.helpdescription = Description;
        [Description release];
        [self addSubview:helpdescription];
        
        UIImageView *ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 42, 28)];
        self.imageview = ImageView;
        [ImageView release];
        [self addSubview:imageview];
        
        UIImageView *DotImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dotted_line_contact.png"]];
        DotImage.frame = CGRectMake(0, 58, 320, 2);
        self.dotimage = DotImage;
        [DotImage release];
        [self addSubview:dotimage];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

-(void)dealloc
{
    [helpdescription release];
    [imageview release];
    [dotimage release];
    [super dealloc];
}

@end
