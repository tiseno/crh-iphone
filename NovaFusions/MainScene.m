//
//  MainScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define SectionTableHeight 75

#import "MainScene.h"

@implementation MainScene

@synthesize sectiontable, imgPicker, searchbar, subview, tapview, bottomview, mainArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bottomview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 64, [[UIScreen mainScreen] bounds].size.width, 44);
    
    [self.sectiontable setBackgroundColor:[UIColor clearColor]];
    UIView *transparentview = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 15)] autorelease];
    transparentview.backgroundColor = [UIColor clearColor];
    self.sectiontable.tableHeaderView = transparentview;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    self.sectiontable.tableFooterView = [[[UIView alloc] init] autorelease];
    
    searchtransform = self.subview.transform;
    self.subview.frame = CGRectMake(0, -[[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    self.subview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    [self.view addSubview:self.subview];
    self.tapview.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closesearch:)];
    [self.tapview addGestureRecognizer:singletap];
    [self.tapview setUserInteractionEnabled:YES];
    
    self.searchbar.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"more_top_bar.png"]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [imgPicker release];
    [sectiontable release];
    [searchbar release];
    [subview release];
    [tapview release];
    [bottomview release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mainArr.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    sectioncell = (SectionCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(sectioncell == nil){
        sectioncell = [[[SectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    
    NSError* error;
    NSDictionary* novainfo = [self.mainArr objectAtIndex:indexPath.row];
    NSDictionary* novadetails = [novainfo objectForKey:@"application"];
    
     NSDictionary *details = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"params"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    sectioncell.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, SectionTableHeight);
    sectioncell.selectionStyle = UITableViewCellSelectionStyleNone;
    sectioncell.title.text = [novadetails objectForKey:@"name"];
    sectioncell.description.text = [details objectForKey:@"content.teaser_description"];
    
    sectioncell.bgview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"category_%d.png", indexPath.row + 1]]];
    return sectioncell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [GlobalFunction AddLoadingScreen:self];
    sectioncell = (SectionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    row = indexPath.row;
    [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    sectioncell = (SectionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return sectioncell.frame.size.height;
}

-(void)delay
{
    NSDictionary* novainfo = [self.mainArr objectAtIndex:row];
    NSDictionary* novadetails = [novainfo objectForKey:@"application"];
    
    WebServices *ws = [[WebServices alloc] init];
    NSArray *test = [ws selectAllFromCategoryTableExceptParam:[[novadetails objectForKey:@"id"] intValue]];
    [ws release];
    
    [GlobalFunction RemoveLoadingScreen:self];
    CategoryScene *GtestClasssViewController=[[[CategoryScene alloc] initWithNibName:@"CategoryScene"  bundle:nil] autorelease];
    GtestClasssViewController.categoryArr = test;
    GtestClasssViewController.categorytext = [novadetails objectForKey:@"name"];
    GtestClasssViewController.row = row;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)myjobs:(id)sender
{
    MyJobScene *GtestClasssViewController=[[[MyJobScene alloc] initWithNibName:@"MyJobScene"  bundle:nil] autorelease];
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)bookmarks:(id)sender
{
    BookmarkScene *GtestClasssViewController=[[[BookmarkScene alloc] initWithNibName:@"BookmarkScene"  bundle:nil] autorelease];
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)search:(id)sender
{
    [self searchviewanimationin];
}

-(IBAction)more:(id)sender
{
    MoreScene *GtestClasssViewController=[[[MoreScene alloc] initWithNibName:@"MoreScene"  bundle:nil] autorelease];
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)scan:(id)sender
{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    reader.readerView.torchMode = 0;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25 config: ZBAR_CFG_ENABLE to: 0];
    
    // present and release the controller
    [self presentModalViewController: reader animated: YES];
    [reader release];
}

-(IBAction)launchsafari:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.crh.com.au" ];
    [[UIApplication sharedApplication] openURL:url];
}

- (void) readerControllerDidFailToRead: (ZBarReaderController*) reader withRetry: (BOOL) retry
{
    NSLog(@"the image picker failing to read");
    
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    NSString *hiddenData;
    for(symbol in results)
        hiddenData=[NSString stringWithString:symbol.data];
    
    NSLog(@"BARCODE= %@",symbol.data);
    
    NSUserDefaults *storeData=[NSUserDefaults standardUserDefaults];
    [storeData setObject:hiddenData forKey:@"CONSUMERID"];
    NSLog(@"SYMBOL : %@",hiddenData);
    
    [reader dismissModalViewControllerAnimated: NO];
    
    data = symbol.data;
    if(data != nil )
    {
        [GlobalFunction AddLoadingScreen:self];
        [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
    }
    else
    {
        data = hiddenData;
        if([data rangeOfString:@"http"].location != NSNotFound)
        {
            NSURL *url = [ [ NSURL alloc ] initWithString:data];
            [[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            [GlobalFunction AddLoadingScreen:self];
            [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
        }
    }
}

//search//
-(void)searchviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, [[UIScreen mainScreen] bounds].size.height);
        self.subview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             searchtransform = self.subview.transform;
                             [self.searchbar becomeFirstResponder];
                         }
                     }];
    [UIView commitAnimations];
}

-(void)searchviewanimationout
{
    self.subview.transform = searchtransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -[[UIScreen mainScreen] bounds].size.height);
        self.subview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             searchtransform = self.subview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)closesearch:(UIGestureRecognizer *)gesture
{
    [self searchviewanimationout];
    [self.searchbar resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar
{
    [self searchviewanimationout];
    [self.searchbar resignFirstResponder];
    [GlobalFunction AddLoadingScreen:self];
    [self performSelector:@selector(delay3) withObject:nil afterDelay:0.1];
    [self.searchbar resignFirstResponder];
}
    
-(void)delay2
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSArray *result = [ws searchItemByBarCode:data];;
    
    if(result.count > 0)
    {
        itemCodeArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemImageArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemNameArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemAppIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        for(int i = 0; i < result.count; i++)
        {
            NSDictionary* novainfo = [result objectAtIndex:i];
            
            NSDictionary* novadetails = [novainfo objectForKey:@"item"];
            
            [itemNameArr addObject:[novadetails objectForKey:@"name"]];
            
            NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the price of the product
            NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
            NSDictionary *price = [priceDetails objectForKey:@"0"];
            
            [itemPriceArr addObject:[price objectForKey:@"value"]];
            
            NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            [itemCodeArr addObject:[itemCode objectForKey:@"value"]];
            
            NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            [itemImageArr addObject:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [itemAppIdArr addObject:[novadetails objectForKey:@"application_id"]];
            
            [itemIdArr addObject:[novadetails objectForKey:@"id"]];
        }
    }
    
    [ws release];
    
    [GlobalFunction RemoveLoadingScreen:self];
    SearchScene *GtestClasssViewController=[[[SearchScene alloc] initWithNibName:@"SearchScene"  bundle:nil] autorelease];
    
    GtestClasssViewController.keyword = self.searchbar.text;
    GtestClasssViewController.itemCodeArr = itemCodeArr;
    GtestClasssViewController.itemImageArr = itemImageArr;
    GtestClasssViewController.itemNameArr = itemNameArr;
    GtestClasssViewController.itemPriceArr = itemPriceArr;
    GtestClasssViewController.itemAppIdArr = itemAppIdArr;
    GtestClasssViewController.itemIdArr = itemIdArr;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
    self.searchbar.text = @"";
}

-(void)delay3
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSArray *result = [ws searchItemByKeywords:self.searchbar.text];
    
    if(result.count > 0)
    {
        itemCodeArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemImageArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemNameArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemAppIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        for(int i = 0; i < result.count; i++)
        {
            NSDictionary* novainfo = [result objectAtIndex:i];
            
            NSDictionary* novadetails = [novainfo objectForKey:@"item"];
            
            [itemNameArr addObject:[novadetails objectForKey:@"name"]];
            
            NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the price of the product
            NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
            NSDictionary *price = [priceDetails objectForKey:@"0"];
            
            [itemPriceArr addObject:[price objectForKey:@"value"]];
            
            NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            [itemCodeArr addObject:[itemCode objectForKey:@"value"]];
            
            NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            [itemImageArr addObject:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [itemAppIdArr addObject:[novadetails objectForKey:@"application_id"]];
            
            [itemIdArr addObject:[novadetails objectForKey:@"id"]];
        }
    }
    
    [ws release];
    
    [GlobalFunction RemoveLoadingScreen:self];
    SearchScene *GtestClasssViewController=[[[SearchScene alloc] initWithNibName:@"SearchScene"  bundle:nil] autorelease];
    
    GtestClasssViewController.keyword = self.searchbar.text;
    GtestClasssViewController.itemCodeArr = itemCodeArr;
    GtestClasssViewController.itemImageArr = itemImageArr;
    GtestClasssViewController.itemNameArr = itemNameArr;
    GtestClasssViewController.itemPriceArr = itemPriceArr;
    GtestClasssViewController.itemAppIdArr = itemAppIdArr;
    GtestClasssViewController.itemIdArr = itemIdArr;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
    self.searchbar.text = @"";
}

/////////////////////

@end
