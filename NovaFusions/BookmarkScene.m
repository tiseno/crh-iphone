//
//  BookmarkScene.m
//  NovaFusions
//
//  Created by tiseno on 10/23/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define BookmarkTableHeight 105
#define BookmarkTableWidth 320

#import "BookmarkScene.h"

@implementation BookmarkScene

@synthesize bookmarktable, titlelabel, editbookmarkbutton, backbutton, priceview, typeview, topbar, bottombar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[UIScreen mainScreen] bounds].size.height > 480)
        self.topbar.frame = CGRectMake(0, 0, 320, 44);
    [self.topbar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"more_top_bar.png"]] autorelease] atIndex:1];
    [self.bottombar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"more_btm_bar.png"]] autorelease] atIndex:1];

    self.bookmarktable.tableFooterView = [[[UIView alloc] init] autorelease];
    self.titlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
    
    db = [[DatabaseAction alloc] init];
    
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
    iseditbookmark = NO;
    
    pricetransform = self.priceview.transform;
    self.priceview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"send_email_background_1.png"]];
    self.priceview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-20, [[UIScreen mainScreen] bounds].size.width, self.priceview.frame.size.height);
    [self.view addSubview:self.priceview];
    
    typetransform = self.typeview.transform;
    self.typeview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"send_email_background_2.png"]];
    self.typeview.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-20, [[UIScreen mainScreen] bounds].size.width, self.typeview.frame.size.height);
    [self.view addSubview:self.typeview];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [bookmarktable release];
    [titlelabel release];
    [editbookmarkbutton release];
    [backbutton release];
    [priceview release];
    [typeview release];
    [topbar release];
    [bottombar release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return bookmarklist.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
    
    bookmarkcell = (BookmarkCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    
    if(bookmarkcell == nil){
        bookmarkcell = [[[BookmarkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
    }
    
    bookmarkcell.frame = CGRectMake(0, 0, BookmarkTableWidth, BookmarkTableHeight);
    bookmarkcell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    bookmarklist = [db retrieveBookmark];
    Bookmark *bookmark = [bookmarklist objectAtIndex:indexPath.row];
    
    bookmarkcell.titlelabel.text = bookmark.bookmarktitle;
    bookmarkcell.codelabel.text = [NSString stringWithFormat:@"(%@)", bookmark.bookmarkcode];
    bookmarkcell.productimage.imageURL = [NSURL URLWithString:bookmark.bookmarkimagepathS];
    
    UIButton *viewbutton = [[[UIButton alloc] initWithFrame:CGRectMake(256, 75, 49, 18)] autorelease];
    viewbutton.tag = indexPath.row;
    [viewbutton setBackgroundImage:[UIImage imageNamed:@"btn_view.png"] forState:UIControlStateNormal];
    [bookmarkcell addSubview:viewbutton];
    
    [viewbutton addTarget:self action:@selector(productdetailview:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(iseditbookmark)
    {
        bookmarkcell.productimage.frame = CGRectMake(30, 0, 83, 104);
        bookmarkcell.titlelabel.frame = CGRectMake(120, 10, 180, 40);
        bookmarkcell.codelabel.frame = CGRectMake(120, 50, 180, 20);
        viewbutton.hidden = YES;
        
        UIButton *deletebutton = [[[UIButton alloc] initWithFrame:CGRectMake(5 , (BookmarkTableHeight - 27) / 2 , 27, 27)] autorelease];
        deletebutton.tag = indexPath.row;
        [deletebutton setBackgroundImage:[UIImage imageNamed:@"btn_delete.png"] forState:UIControlStateNormal];
        [bookmarkcell addSubview:deletebutton];
        
        [deletebutton addTarget:self action:@selector(deletebookmark:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        bookmarkcell.productimage.frame = CGRectMake(0, 0, 83, 104);
        bookmarkcell.titlelabel.frame = CGRectMake(90, 10, 210, 40);
        bookmarkcell.codelabel.frame = CGRectMake(90, 50, 210, 20);
        viewbutton.hidden = NO;
    }
    
    return bookmarkcell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    bookmarkcell = (BookmarkCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return bookmarkcell.frame.size.height;
}

-(IBAction)productdetailview:(id)sender
{
    ProductScene *GtestClasssViewController=[[[ProductScene alloc] initWithNibName:@"ProductScene"  bundle:nil] autorelease];
    bookmarklist = [db retrieveBookmark];
    Bookmark *bookmark = [bookmarklist objectAtIndex:[sender tag]];
    
    GtestClasssViewController.appID = [NSString stringWithFormat:@"%d", bookmark.sectionid];
    GtestClasssViewController.itemID = [NSString stringWithFormat:@"%d",bookmark.categoryid];
    GtestClasssViewController.isjumpfromsearch = YES;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)editbookmark:(id)sender
{
    if(iseditbookmark)
    {
        [self.editbookmarkbutton setBackgroundImage:[UIImage imageNamed:@"btn_edit.png"] forState:UIControlStateNormal];
        self.backbutton.enabled = YES;
    }
    else
    {
        [self.editbookmarkbutton setBackgroundImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
        self.backbutton.enabled = NO;
    }
    iseditbookmark = !iseditbookmark;
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)deletebookmark:(id)sender
{
    int bookmarkid = [db retrieveBookmarkselectedrow:[sender tag]];
    [db deleteBookmark:bookmarkid];
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
}

-(IBAction)email:(id)sender
{
    bookmarklist = [db retrieveBookmark];
    if(bookmarklist.count > 0)
        [self priceviewanimationin];
}

-(IBAction)cancel:(id)sender
{
    if([sender tag] == 988)
    {
        [self priceviewanimationout];
    }
    else
    {
        [self typeviewanimationout];
        [self priceviewanimationin];
    }
}

-(IBAction)sendwithprice:(id)sender
{
    isneedprice = YES;
    [self priceviewanimationout];
    [self performSelector:@selector(typeviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)sendwithoutprice:(id)sender
{
    isneedprice = NO;
    [self priceviewanimationout];
    [self performSelector:@selector(typeviewanimationin) withObject:nil afterDelay:0.4];
}

-(IBAction)sendplain:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSMutableString *subject = [[NSMutableString alloc] init];
        NSMutableString *emailBody = [[NSMutableString alloc] init];
        
        [subject appendString:@"CRH - My Bookmarks"];
        
        bookmarklist = [db retrieveBookmark];
        
        if(isneedprice)
        {
            for(int i = 0; i < bookmarklist.count; i++)
            {
                Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle], [NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], [NSString stringWithFormat:@"Trade: %@", bookmark.bookmarktrade], [NSString stringWithFormat:@"Retail: %@", bookmark.bookmarkretail], bookmark.bookmarkdescription];
                
                [emailBody appendString:appStr];
                
                if((i+1) == bookmarklist.count)
                {
                    [emailBody appendString:@"\n\n\n"];
                }
            }
        }
        else
        {
            for(int i = 0; i < bookmarklist.count; i++)
            {
                Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle], [NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], bookmark.bookmarkdescription];
                
                [emailBody appendString:appStr];
                
                if((i+1) == bookmarklist.count)
                {
                    [emailBody appendString:@"\n\n\n"];
                }
            }
        }
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:subject];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        [mailer setMessageBody:emailBody isHTML:NO];
        
        // only for iPad
        mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
        [subject release];
        [emailBody release];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
}

-(IBAction)sendhtml:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSMutableString *subject = [[NSMutableString alloc] init];
        NSMutableString *emailBody = [[NSMutableString alloc] init];
        
        [subject appendString:@"CRH - My Bookmarks"];
        
        bookmarklist = [db retrieveBookmark];
        
        if(isneedprice)
        {
            for(int i = 0; i < bookmarklist.count; i++)
            {
                Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                
                NSString *filterNewLine = [NSString stringWithFormat:@"%@", [bookmark.bookmarkdescription  stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@", bookmark.bookmarkimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle],[NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], [NSString stringWithFormat:@"Trade : %@", bookmark.bookmarktrade], [NSString stringWithFormat:@"Retail : %@", bookmark.bookmarkretail], filterNewLine];
                
                [emailBody appendString:appStr];
                
                if((i+1) == bookmarklist.count)
                {
                    [emailBody appendString:@"<br /><br /><br />"];
                }
            }
        }
        else
        {
            for(int i = 0; i < bookmarklist.count; i++)
            {
                Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                
                NSString *filterNewLine = [NSString stringWithFormat:@"%@", [bookmark.bookmarkdescription  stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
                
                NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@", bookmark.bookmarkimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle], [NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], filterNewLine];
                
                [emailBody appendString:appStr];
                
                if((i+1) == bookmarklist.count)
                {
                    [emailBody appendString:@"<br /><br /><br />"];
                }
            }
        }
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:subject];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        [mailer setMessageBody:emailBody isHTML:YES];
        
        // only for iPad
        mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
        [subject release];
        [emailBody release];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
}

-(IBAction)sendpdf:(id)sender
{
    [GlobalFunction AddLoadingScreen:self];
    [self.view makeToast:@"Generating PDF..." duration:(0.3) position:@"center"];
    [self performSelector:@selector(delayPDF) withObject:nil afterDelay:0.3];
}

-(void)delayPDF
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSString * timeStampValue = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
        
        NSMutableString *subject = [[NSMutableString alloc] init];
        
        NSMutableString *requestString = [[NSMutableString alloc] init];
        
        WebServices *ws = [[WebServices alloc] init];
        
        NSString *reply = [[NSString alloc] init];
        
        NSMutableString *emailBody = [[NSMutableString alloc] init];
        
        [subject appendString:@"CRH - My Bookmarks"];
        
        bookmarklist = [db retrieveBookmark];
        
        if(isneedprice)
        {
            [requestString appendString:@"["];
            
            for(int i = 0; i < bookmarklist.count; i++)
            {
                Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                
                NSString *filterStr = [bookmark.bookmarkimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                
                NSString *filterDescStr = [bookmark.bookmarkdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"µm" withString:@"micrometer"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"©" withString:@"(copyrighted)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
                
                NSString *filterTitleStr = [bookmark.bookmarktitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                
                NSString *bookmarkString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Trade\":\"%@\",\"Retail\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\"}", filterTitleStr, bookmark.bookmarkcode, bookmark.bookmarktrade, bookmark.bookmarkretail, filterDescStr, filterStr, timeStampValue];
                
                [requestString appendString:bookmarkString];
                
                if((i+1) != bookmarklist.count)
                {
                    [requestString appendString:@","];
                }
            }
            
            [requestString appendString:@"]"];
            
            reply = [ws generateListOfItemsPDFWithPrice:requestString];
        }
        else
        {
            [requestString appendString:@"["];
            
            for(int i = 0; i < bookmarklist.count; i++)
            {
                Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                
                NSString *filterStr = [bookmark.bookmarkimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                
                NSString *filterDescStr = [bookmark.bookmarkdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"µm" withString:@"micrometer"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"©" withString:@"(copyrighted)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
                
                NSString *filterTitleStr = [bookmark.bookmarktitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"¼" withString:@" 1/4"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"½" withString:@" 1/2"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"²" withString:@" (superscript 2)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"³" withString:@" (superscript 3)"];
                filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"®" withString:@"(registered trademark)"];
                
                NSString *bookmarkString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\"}", filterTitleStr, bookmark.bookmarkcode, filterDescStr, filterStr, timeStampValue];
                
                [requestString appendString:bookmarkString];
                
                if((i+1) != bookmarklist.count)
                {
                    [requestString appendString:@","];
                }
            }
            
            [requestString appendString:@"]"];
            
            reply = [ws generateListOfItemsPDFWithoutPrice:requestString];
        }
        
        if([reply isEqualToString:@"Success"])
        {
            NSString *pdf_link = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/tiseno/fpdf17/product_pdf/%@.pdf", timeStampValue];
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:subject];
            
            [emailBody appendString:[NSString stringWithFormat:@"<br /><br /><br /><br />Click the link to download the pdf file : <a href=\"%@\">%@.pdf</a>", pdf_link, timeStampValue]];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            [mailer setMessageBody:emailBody isHTML:YES];
            
            // only for iPad
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
            [subject release];
            [ws release];
            [reply release];
            [emailBody release];
        }
        else
        {
            [self.view makeToast:@"Failed to generate PDF..." duration:(0.3) position:@"center"];
        }
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
    
    [GlobalFunction RemoveLoadingScreen:self];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)priceviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -self.priceview.frame.size.height);
        self.priceview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             pricetransform = self.priceview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)priceviewanimationout
{
    self.priceview.transform = pricetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, self.priceview.frame.size.height);
        self.priceview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             pricetransform = self.priceview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)typeviewanimationin
{
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -self.typeview.frame.size.height);
        self.typeview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             typetransform = self.typeview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)typeviewanimationout
{
    self.typeview.transform = typetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, self.typeview.frame.size.height);
        self.typeview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             typetransform = self.typeview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

@end
