//
//  InfoJobCell.h
//  NovaFusions
//
//  Created by tiseno on 10/30/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *titlelabel, *codelabel, *tradelabel, *retaillabel, *quantitylabel;
@property (nonatomic, retain) UIImageView *productimage, *quantityimage;

@end
