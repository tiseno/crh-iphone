//
//  JobCell.m
//  NovaFusions
//
//  Created by tiseno on 10/25/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "JobCell.h"

@implementation JobCell

@synthesize jobtitle, jobdescription;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *jobTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 150, 15)];
        jobTitle.textAlignment = UITextAlignmentLeft;
        jobTitle.textColor = [UIColor blackColor];
        jobTitle.backgroundColor = [UIColor clearColor];
        jobTitle.font = [UIFont boldSystemFontOfSize:13];
        jobTitle.numberOfLines = 0;
        self.jobtitle = jobTitle;
        [jobTitle release];
        [self addSubview:jobtitle];
        
        UILabel *jobDescription = [[UILabel alloc] initWithFrame:CGRectMake(10, 23, 150, 12)];
        jobDescription.textAlignment = UITextAlignmentLeft;
        jobDescription.textColor = [UIColor grayColor];
        jobDescription.backgroundColor = [UIColor clearColor];
        jobDescription.font = [UIFont systemFontOfSize:11];
        jobDescription.lineBreakMode = UILineBreakModeWordWrap;
        jobDescription.numberOfLines = 1;
        self.jobdescription = jobDescription;
        [jobDescription release];
        [self addSubview:jobdescription];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [jobtitle release];
    [jobdescription release];
    [super dealloc];
}

@end
