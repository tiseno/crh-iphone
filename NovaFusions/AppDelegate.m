//
//  AppDelegate.m
//  NovaFusions
//
//  Created by tiseno on 10/22/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

@synthesize colorcodered, colorcodegreen, colorcodeblue, helpdescription;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.colorcodered = [[NSMutableArray alloc]init];
    red = [NSNumber numberWithFloat:243.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:250.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:241.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:229.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:215.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:217.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:189.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:135.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:99.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:69.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:49.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:40.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:29.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:53.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:102.0];
    [self.colorcodered addObject:red];
    red = [NSNumber numberWithFloat:171.0];
    [self.colorcodered addObject:red];
    [self.colorcodered release];
    
    self.colorcodegreen = [[NSMutableArray alloc]init];
    green = [NSNumber numberWithFloat:214.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:187.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:123.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:79.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:53.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:36.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:34.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:47.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:72.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:82.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:90.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:115.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:130.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:149.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:174.0];
    [self.colorcodegreen addObject:green];
    green = [NSNumber numberWithFloat:201.0];
    [self.colorcodegreen addObject:green];
    [self.colorcodegreen release];
    
    self.colorcodeblue = [[NSMutableArray alloc]init];
    blue = [NSNumber numberWithFloat:57.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:43.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:26.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:45.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:69.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:61.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:141.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:168.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:177.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:185.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:188.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:198.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:160.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:121.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:74.0];
    [self.colorcodeblue addObject:blue];
    blue = [NSNumber numberWithFloat:61.0];
    [self.colorcodeblue addObject:blue];
    [self.colorcodeblue release];
    
    self.gradientbottom = [[NSMutableArray alloc]init];
    bottom = (id)[[UIColor colorWithRed:207.0/255.0 green:182.0/255.0 blue:49.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:213.0/255.0 green:159.0/255.0 blue:37.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:205.0/255.0 green:105.0/255.0 blue:22.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:195.0/255.0 green:67.0/255.0 blue:38.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:183.0/255.0 green:45.0/255.0 blue:59.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:185.0/255.0 green:31.0/255.0 blue:52.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:161.0/255.0 green:29.0/255.0 blue:120.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:115.0/255.0 green:40.0/255.0 blue:143.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:84.0/255.0 green:61.0/255.0 blue:151.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:59.0/255.0 green:70.0/255.0 blue:157.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:42.0/255.0 green:77.0/255.0 blue:160.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:34.0/255.0 green:98.0/255.0 blue:168.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:136.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:45.0/255.0 green:127.0/255.0 blue:103.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:87.0/255.0 green:148.0/255.0 blue:63.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    bottom = (id)[[UIColor colorWithRed:146.0/255.0 green:171.0/255.0 blue:52.0/255.0 alpha:1] CGColor];
    [self.gradientbottom addObject:bottom];
    [self.gradientbottom release];
    
    self.helpdescription = [[NSMutableArray alloc]init];
    descriptiontext = @"Tap this icon to show the search bar.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to add an item to your bookmarks.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to view larger image of a product.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to view technical drawing of a product.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to view thumbnail format.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to email bookmarked item or jobs (with price or without price) as plain text, HTML or PDF format.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to rename or delete a job or Bookmark.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap to access barcode scanner. You may use this feature to scan any CRH Australia product barcode and obtain the product details.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this button to bring up the job menu for an item You may add any number or the item to one of your jobs.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to access the product enquiry form.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this button to email product detail (with price or without price) as plain text, HTML or PDF format.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to access the bookmark view. You may view bookmarked products and share the details.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to access the Job view. You may create, edit, view or delete a job.";
    [self.helpdescription addObject:descriptiontext];
    descriptiontext = @"Tap this icon to visit CRH Australia website. Internet connection is needed for this function.";
    [self.helpdescription addObject:descriptiontext];
    [self.helpdescription release];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
